#include<stdio.h>
int atoi(char* a)
{
    int i,n=0;
    for(i=0;(a[i]>='0'&&a[i]<='9');i++) n=n*10+(a[i]-'0');
    return n;
}
int main(int argc, char* argv[], char* envp[]) {
    int t;
    if(argc) {
              t=atoi(argv[0]);
              if(!t) printf("Usage: sleep s, where s is the number of seconds\n");
              else sleep(t);
              //printf("Slept for %d seconds\n",t);
    }
    else {
          printf("Usage: sleep s, where s is the number of seconds\n");
         }
    return 0;
}
