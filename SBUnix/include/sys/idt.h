#ifndef _IDT_H
#define _IDT_H

#include <defs.h>
#define INTERRUPT 0x0e /*** automatically disable interrupts on entry ***/
#define TRAP_GATE 0x0f /*** allow interrupts to continue on entry ***/

#define PRESENT_ISR(selector, ist, type, dpl, target) { \
        (target) & 0x0000ffff, \
        selector, \
        ist, \
        0, \
        type, \
        0, \
        dpl, \
        1, \
        ((target) >> 16) & 0x0000ffff, \
        ((target) >> 32) & 0xffffffff, \
        0 \
	}

#define ABSENT_ISR(selector, ist, type, dpl, target) { \
        0, \
        selector, \
        ist, \
        0, \
        type, \
        0, \
        dpl, \
        0, \
        0, \
        0, \
        0 \
	}
void reload_idt();

#endif
