#ifndef _STDIO_H
#define _STDIO_H

#define wait(x) waitpid(-1,x)
void putchar(char c);
int printf(const char *format, ...);
int scanf(const char *format, ...);
int getpid();
void* malloc(unsigned long);
int fork();
int execvpe(const char *file, char * argv[], char * envp[]);
unsigned int sleep(unsigned int);
unsigned int waitpid(int pid, int *status);
int read(int,char*,int);
int write(int,char*,int);

int opendir(char* dname);
int readdir(int);
int closedir(int);
int printdirentry(int);

void getCurrentDirectory(char*);
void setCurrentDirectory(char*);

int open(char* fname);
int close(int);

void listProcesses();
#endif
