#ifndef _SYSCALL_H
#define _SYSCALL_H

#include <defs.h>
//#include <stdio.h>
#define SYSCALL_PROTO(n) static __inline uint64_t __syscall##n

#define SYS_PUTCHAR 1
#define SYS_EXIT 2
#define SYS_MALLOC 3
#define SYS_GETPID 4
#define SYS_FORK 5
#define SYS_EXECVPE 6
#define SYS_SLEEP 7
#define SYS_WAITPID 8
#define SYS_READ 9
#define SYS_WRITE 10
#define SYS_OPENDIR 11
#define SYS_READDIR 12
#define SYS_CLOSEDIR 13
#define SYS_PRINTDIRENT 14
#define SYS_GETCURRDIR 15
#define SYS_SETCURRDIR 16
#define SYS_OPENFILE 17
#define SYS_CLOSEFILE 18
#define SYS_LISTPROCESSES 19
//uint64_t ret;

SYSCALL_PROTO(0)(uint64_t n) {
	uint64_t ret;
    /*asm volatile("pushq %rax");
    //asm volatile("pushq %rax;pushq %rbx;pushq %rcx;pushq %rdx;pushq %rsi;pushq %rdi;pushq %r8;pushq %r9;pushq %r10;pushq %r11");
    asm volatile("movq %0, %%rax":: "b"(n));
    asm volatile("int $0x80");
    asm volatile("movq %%rax, %0;" : "=g"(ret));
    //asm volatile("popq %r11;popq %r10;popq %r9;popq %r8;popq %rdi;popq %rsi;popq %rdx;popq %rcx;popq %rbx;popq %rax");
    asm volatile("popq %rax");*/
    __asm volatile("movq %1,%%rax;"
                   "int $0x80;"
                   "movq %%rax,%0;"
                   :"=r"(ret)
                   :"r"(n)
                   :"rax"//Clobberred registers
                   );
    return ret;
}

SYSCALL_PROTO(1)(uint64_t n, uint64_t a1) {
	uint64_t ret;
    /*asm volatile("pushq %rax");
    asm volatile("pushq %rsi");
    asm volatile("movq %0, %%rax":: "b"(n));
    asm volatile("movq %0, %%rsi":: "b"(a1));
    asm volatile("int $0x80");
    asm volatile("movq %%rax, %0;" : "=g"(ret));
    asm volatile("popq %rsi");
    asm volatile("popq %rax");*/
    __asm volatile("movq %1,%%rax;"
                   "movq %2,%%rsi;"
                   "int $0x80;"
                   "movq %%rax,%0;"
                   :"=r"(ret)
                   :"r"(n),"r"(a1)
                   :"rax","rsi"//Clobberred registers
                   );
    return ret;
}

/*SYSCALL_PROTO(2)(uint64_t n) {
	//uint64_t ret;
    //asm volatile("pushq %rax");
    asm volatile("pushq %rax;pushq %rbx;pushq %rcx;pushq %rdx;pushq %rsi;pushq %rdi;pushq %r8;pushq %r9;pushq %r10;pushq %r11");
    asm volatile("movq %0, %%rax":: "b"(n));
    asm volatile("int $0x80");
    asm volatile("movq %%rax, %0;" : "=g"(ret));
    printf("SYSCALL2: returning %d\n",ret);
    asm volatile("popq %r11;popq %r10;popq %r9;popq %r8;popq %rdi;popq %rsi;popq %rdx;popq %rcx;popq %rbx;popq %rax");
    //asm volatile("popq %rax");
    return 0;
}*/

SYSCALL_PROTO(2)(uint64_t n, uint64_t a1, uint64_t a2) {
    uint64_t ret;
    __asm volatile("movq %1,%%rax;"
                   "movq %2,%%rsi;"
                   "movq %3,%%rdi;"
                   "int $0x80;"
                   "movq %%rax,%0;"
                   :"=r"(ret)
                   :"r"(n),"r"(a1),"r"(a2)
                   :"rax","rsi","rdi"//Clobberred registers
                   );
    return ret;
}

SYSCALL_PROTO(3)(uint64_t n, uint64_t a1, uint64_t a2, uint64_t a3) {
	 uint64_t ret;
    __asm volatile("movq %1,%%rax;"
                   "movq %2,%%rsi;"
                   "movq %3,%%rdi;"
                   "movq %4,%%rdx;"
                   "int $0x80;"
                   "movq %%rax,%0;"
                   :"=r"(ret)
                   :"r"(n),"r"(a1),"r"(a2),"r"(a3)
                   :"rax","rsi","rdi","rdx"//Clobberred registers
                   );
    return ret;
}

SYSCALL_PROTO(4)(uint64_t n, uint64_t a1, uint64_t a2, uint64_t a3, uint64_t a4) {
	return 0;
}

void dummy_call(uint64_t k);
#endif
