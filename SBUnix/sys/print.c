#define SCREEN_WIDTH_BYTES 160
#define TABSPACE_BYTES 8

/* Reference: http://wiki.osdev.org/Printing_To_Screen#printf_and_variable_argument_lists */
#define va_start(v,l) __builtin_va_start(v,l)
#define va_arg(v,l)   __builtin_va_arg(v,l)
#define va_end(v)     __builtin_va_end(v)
#define va_copy(d,s)  __builtin_va_copy(d,s)
typedef __builtin_va_list va_list;

void putchar(char c)
{
 static volatile int printpos = 0;
 volatile register char *pos,*pos1;
 int i;
 pos = pos1 = (char*)0xFFFFFFFF80300000;
 if(printpos>=4000) {
                     for(i=0;i<=3839;i++) {
                                           *(pos+i)=*(pos+i+160);
                                          }
                     for(i=3840;i<4000;i++) *(pos+i)='\0';
                     printpos = 3840;
                    }
 pos+=printpos;
 *(pos+1)=0x07;
 if(c=='\n')
 {
	printpos=((printpos+SCREEN_WIDTH_BYTES)/SCREEN_WIDTH_BYTES)*SCREEN_WIDTH_BYTES;
 }
 else if(c=='\t')
 {
    printpos=printpos+TABSPACE_BYTES;
    if(printpos%SCREEN_WIDTH_BYTES<TABSPACE_BYTES) {
                                                    printpos = (printpos/SCREEN_WIDTH_BYTES)*SCREEN_WIDTH_BYTES;
    }
 }
 else if(c=='\b')
 {
    *pos=0;
    if(printpos>=2) printpos=printpos-2;
 }
 else if(c=='\r')
 {
    printpos=(printpos/SCREEN_WIDTH_BYTES)*SCREEN_WIDTH_BYTES;
 }
 else
 {
	*pos = c;
    printpos+=2;
 }
 *(pos1+printpos+1)=0x77;
}

void puts(char * s)
{
    for(;*s!=0;s++) putchar(*s);
}

void putint(int num)
{
     if(num/10==0)
     {
      putchar('0'+num);
     }
     else
     {
      putint(num/10);
      putchar('0'+num%10);
     }
}
void puthex(unsigned int num)
{
     char MAP[17]="0123456789abcdef";
     if(num/16==0) 
                   putchar(MAP[num]);
     else {
           puthex(num/16);
           putchar(MAP[num%16]);
          }
}
void putaddr(unsigned long num)
{
     char MAP[17]="0123456789abcdef";
     if(num/16==0) 
                   putchar(MAP[num]);
     else {
           putaddr(num/16);
           putchar(MAP[num%16]);
          }
}
int printf(const char *format,...)
{
    int i;
    va_list arguments;
    va_start(arguments,format);
    for(i=0;format[i]!=0;i++)
    {
     if(format[i]=='%') {
                           i++;
                           if(format[i]=='c')
                           {
                            putchar(va_arg(arguments,int));
                           }
                           else if(format[i]=='d')
                           {
                            int arg = va_arg(arguments,int);
                            if(arg<0) {arg=-arg;putchar('-');}
                            putint(arg);
                           }
                           else if(format[i]=='x')
                           {
                            puthex(va_arg(arguments,unsigned int));
                           }
                           else if(format[i]=='s')
                           {
                            puts(va_arg(arguments,char*));
                           }
                           else if(format[i]=='p')
                           {
                            puts("0x");
                            unsigned long arg1 = va_arg(arguments,unsigned long);
                            putaddr(arg1);
                            //putaddr(va_arg(arguments,unsigned long));
                           }
     }
     else putchar(format[i]);
    }
    va_end(arguments);
    return 0;
}
