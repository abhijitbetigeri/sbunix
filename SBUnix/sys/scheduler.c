#include <defs.h>
#include <sys/gdt.h>
#include <sys/tarfs.h>
#include "scheduler.h"
#include "memorymanager.h"
#include "virtmemorymanager.h"
#include "elf_header.h"
#include "directory.h"

extern int printf(const char *format,...);
extern void putchar(char);
uint64_t* prev_stack, *prev_stack_t;

extern struct task_struct* kernel_task_struct;
extern uint64_t* bump_ptr;

extern void _isr_032();
extern void PIC_sendEOI(unsigned char);

int prev,next,current=0;

char exit_st=0;
uint64_t return_address=0; // For fork
uint64_t return_stack_addr=0; // For fork
uint64_t rbx,rcx,rdx,rsi,rdi,rbp,r8,r9,r10,r11,r12,r13,r14,r15;
uint32_t forked_cid=0;// For forked child

/* Global Variables for execvpe*/
char kfn[100];
char* kfn1;
char kargvaddr1[10][100];
char* kargvaddr2[10];
char kenvpaddr1[10][100];
char* kenvpaddr2[10];
char checkList[10][100];
struct task_struct* PCB;
uint64_t tempPCB;
//uint64_t stack_to_free;
/*Global Variables for wait */
int zchild=0;
int equal(char* a, char* b)
{
    int i,j;
    for(i=0;a[i];i++);
    for(j=0;b[j];j++);
    if(i!=j) return 0;
    //printf(a);
    //printf(" compared with ");
    //printf(b);
    for(i=0;a[i];i++) if(a[i]!=b[i]) return 0;
    return 1;
}
int pow(int a,int b)
{
    if(b==0) return 1;
    else return a*pow(a,b-1);
}
int atoi(char* a)
{
    int i,n=0;
    for(i=0;a[i];i++) n=n*10+(a[i]-'0');
    return n;
}
int otod(int octal)
{
    int decimal=0,i=0;
    while(octal!=0){
         decimal = decimal + (octal % 10) * pow(8,i);
         octal = octal/10;
         i++;
    }
    return decimal;
}

int create_process_exec(int self,int parent,char* zddr,char* proc_name,char** argv,char** envp) // creates a process and returns pid
{
    struct task_struct* process_task_struct;
    uint32_t proc_id = 0,i,k;
    uint32_t t;
    uint64_t temp_cr3;
    //uint64_t* addr;
    //printf("ProcNAME: %s\n",proc_name);
    /*printf("ARGV:-\n");
    for(i=0;argv[i]!=NULL;i++) printf("%s\n",argv[i]);
    printf("ENVP:-\n");
    for(i=0;envp[i]!=NULL;i++) printf("%s\n",envp[i]);*/
    process_task_struct = (struct task_struct*)pcb_malloc(200); //  1 page for pcb
    
    alloc_mm_struct(process_task_struct); // mm struct shares the same page as pcb
    process_task_struct->mm->mmap=NULL;
    process_task_struct->mm->map_count=0;
    
    if(!process_task_struct) return -1; // out of memory
    
    uint64_t* pml4 = pcb_malloc(200); // just need 1 page for the pml4 table of the new process
    if(!pml4) return -1; //out of memory
    
    pml4[511] = *(getPML4E((uint64_t*)(0xffffffff80200000))); // making the last entry point to the PDPE for kernel
    pml4[500]=ALIGN_DOWN((uint64_t)(*getPTE(pml4))) | PT_PRESENT_FLAG | PT_WRITABLE_FLAG; // self reference
    
    for(i=0;i<MAX_PROCESS_COUNT;i++) if(process_queue[i]==0) break;
    if(i==MAX_PROCESS_COUNT) return -1; // too many processes
    
    proc_id=i;
    //process_task_struct->pid = proc_id;
    process_task_struct->pid = self; // in exec, this field won't change
    for(i=0;proc_name[i]!=0;i++) process_task_struct->procName[i]=proc_name[i];
    process_task_struct->procName[i]=0;
    process_task_struct->pml4_vaddr = (uint64_t)pml4;
    process_queue[proc_id]=(uint64_t)process_task_struct;
    
    process_task_struct->cr3 = ALIGN_DOWN((uint64_t)(*getPTE(pml4)));
    
    process_task_struct->is_zombie=0;
    process_task_struct->exit_status=0; // this field is ignored until the process exits
    
    process_task_struct->ulimit=10240; // stack size. Defaults to 10KB
    
    temp_cr3 = ((struct task_struct*)(process_queue[self]))->cr3;
    //printf("Switching CR3\n");
    // Switch to process virtual address space
    asm volatile("movq %0, %%cr3":: "b"(process_task_struct->cr3));
    //asm volatile( "sti");
    //printf("DEBUG: Virtual Address Space successfully switched\n");
    process_task_struct->parent = parent;
    process_task_struct->stack = (uint64_t*)(0xffffff8000000000-process_task_struct->ulimit); // allocating stack in higher most possible memory
    //process_task_struct->stack = (uint64_t*)(0xffffff8000000000)-512;
    //printf("Stack: %p\n",process_task_struct->stack);
    process_task_struct->kernel_stack = pcb_malloc(4097); // we need 2 pages
    process_task_struct->kernel_rsp = &process_task_struct->kernel_stack[1023];  // will be changed
       
    process_task_struct->kernel_stack[1023] = process_task_struct->kernel_stack[1022] = process_task_struct->kernel_stack[1021] = process_task_struct->kernel_stack[1020] = process_task_struct->kernel_stack[1019] = 0;
    process_task_struct->kernel_stack[1022] = (uint64_t)(0x23); // ss should be 0x23
    process_task_struct->kernel_stack[1021] = (uint64_t)(process_task_struct->stack)+process_task_struct->ulimit-1; // the user stack
    //process_task_struct->kernel_stack[1021] = (uint64_t)(&process_task_struct->stack[511]);
    //printf("USER_STACK_BOTTOM: %p\n",process_task_struct->kernel_stack[1021]);
    //process_task_struct->stack[509] = stack2;
    process_task_struct->kernel_stack[1020] = 0x00200200;
    process_task_struct->kernel_stack[1019] = (uint64_t)(0x1b); //cs should be 0x1b
    //process_task_struct->stack[509] = 0x200046;
    // process_task_struct->stack[506] the entry point will be defined here
    process_task_struct->kernel_stack[1017] = 0; //rax
    process_task_struct->kernel_stack[1016] = 0; //rbx
    process_task_struct->kernel_stack[1015] = 0; //rcx
    process_task_struct->kernel_stack[1014] = 0; //rdx -- third argument
    process_task_struct->kernel_stack[1013] = 0; //rsi -- second argument
    process_task_struct->kernel_stack[1012] = 0; //rdi -- first argument
    process_task_struct->kernel_stack[1011] = 0; //rbp
    process_task_struct->kernel_stack[1010] = 0; //r8
    process_task_struct->kernel_stack[1009] = 0; //r9
    process_task_struct->kernel_stack[1008] = 0; //r10
    process_task_struct->kernel_stack[1007] = 0; //r11
    process_task_struct->kernel_stack[1006] = 0; //r12
    process_task_struct->kernel_stack[1005] = 0; //r13
    process_task_struct->kernel_stack[1004] = 0; //r14
    process_task_struct->kernel_stack[1003] = 0; //r15
    process_task_struct->kernel_stack[1002] = (uint64_t)(_isr_032+40);
    process_task_struct->kernel_stack[1001] = 0; // extra rbp pop
    process_task_struct->kernel_rsp = &(process_task_struct->kernel_stack[1001]);
    
    char* zddr1 = zddr;
    struct elf_header* elf = (struct elf_header*)zddr;
    uint16_t phnum = elf->phnum,j;
    uint16_t phsize = elf->phentsize;
    uint64_t phoff = elf->phoff;
    //process_task_struct->stack[506] = elf->entry;
    process_task_struct->kernel_stack[1018] = elf->entry;
    zddr+=phoff;
    //uint64_t k;
    uint64_t brk_begin=0x5000; // just any initial value
    //printf("elf->entry = %p\n",elf->entry);
    //printf("[SYSTEM_MESSAGE]Creating VMAs\n");
    for(j=0;j<phnum;j++) {
                          struct prog_header* pheader = (struct prog_header*)zddr;
                          uint32_t type = pheader->p_type;
                          uint32_t flags = pheader->p_flags;
                          uint64_t vaddr = pheader->p_vaddr;
                          uint64_t offset = pheader->p_offset;
                          uint64_t size = pheader->p_memsz;
                          //printf("type=%p vaddr=%p offset=%p size=%p\n",type,vaddr,offset,size);
                          if(type==PT_LOAD) // check for LOAD type
                          {
                           //printf("[SYSTEM_MESSAGE]type=%p vaddr=%p offset=%p size=%p\n",type,vaddr,offset,size);
                           struct vm_area_struct* vma = alloc_vma(process_task_struct->mm);
                           vma->vm_mm=process_task_struct->mm;
                           vma->vm_start = vaddr;
                           vma->vm_end = (uint64_t)((char*)vaddr+size);
                           brk_begin = vma->vm_end;
                           vma->vm_memsz = size;
                           vma->vm_pgoff = offset;
                           vma->vm_next = NULL;
                           vma->vm_filestart = (uint64_t)zddr1;
                           vma->vm_flags=(uint64_t)flags;
                          }
                          zddr+=phsize;
                         }
    
    struct vm_area_struct* vma;
    
    vma = alloc_vma(process_task_struct->mm); // a vma for storing argv and envp
    vma->vm_mm = process_task_struct->mm;
    vma->vm_start = ALIGN_DOWN(brk_begin+0x1000);
    vma->vm_end = ALIGN_DOWN(brk_begin+0x2000);
    vma->vm_memsz = 0;
    vma->vm_pgoff = 0;
    vma->vm_next = NULL;
    vma->vm_filestart = NULL;
    vma->vm_flags = 0 | VM_READ | VM_WRITE | VM_GROWSDOWN;
    t=back_with_physical((uint64_t*)(vma->vm_start)); // allocating this address now itself
    if(!t) return -1;
    volatile uint64_t* pml4e = getPML4E((uint64_t*)(vma->vm_start));
    volatile uint64_t* pdpe = getPDPE((uint64_t*)(vma->vm_start));
    volatile uint64_t* pde = getPDE((uint64_t*)(vma->vm_start));
    volatile uint64_t* pte = getPTE((uint64_t*)(vma->vm_start));
    (*pte) = (*pte) | PT_USER_FLAG;
    (*pde) = (*pde) | PT_USER_FLAG;
    (*pdpe) = (*pdpe) | PT_USER_FLAG;
    (*pml4e) = (*pml4e) | PT_USER_FLAG;
    char* cla_addr=(char*)vma->vm_start;
    process_task_struct->kernel_stack[1012] = (uint64_t)cla_addr; // first argument goes to rdi -- addr of start of argv
    for(i=0;argv[i]!=NULL;i++) {
                                //printf("<i=%d>",i);
                                for(k=0;argv[i][k]!=0;k++) {
                                                            *cla_addr=argv[i][k];
                                                            //printf("%p %c ",cla_addr,*cla_addr);
                                                            cla_addr++;
                                                           }
                                *cla_addr='\0';
                                cla_addr++;
                               }
    process_task_struct->kernel_stack[1013] = (uint64_t)cla_addr; // second argument goes to rsi -- addr of start of envp
    for(i=0;envp[i]!=NULL;i++) {
                                for(k=0;envp[i][k]!=0;k++) {
                                                            *cla_addr=envp[i][k];
                                                            //printf("%p %c ",cla_addr,*cla_addr);
                                                            cla_addr++;
                                                           }
                                *cla_addr='\0';
                                cla_addr++;
                               }
    process_task_struct->kernel_stack[1014] = (uint64_t)cla_addr; // third argument goes to rdx -- addr of end of envp
    
    vma = alloc_vma(process_task_struct->mm); // a vma for heap1
    vma->vm_mm = process_task_struct->mm;
    vma->vm_start = ALIGN_DOWN(brk_begin+0x2000);
    vma->vm_end = ALIGN_DOWN(brk_begin+0x2000);
    vma->vm_memsz = 0;
    vma->vm_pgoff = 0;
    vma->vm_next = NULL;
    vma->vm_filestart = NULL;
    vma->vm_flags = 0 | VM_READ | VM_WRITE;
    
    /*vma = alloc_vma(process_task_struct->mm); // an area for passing arguments
    vma->vm_mm = process_task_struct->mm;
    vma->vm_start = 0x0000fa8000000000; // next address after 500th PML4 entry
    vma->vm_end = 0x0000fa8000000000+0x1000;
    vma->vm_memsz = 0;
    vma->vm_pgoff = 0;
    vma->vm_next = NULL;
    vma->vm_filestart = NULL;
    vma->vm_flags = 0 | VM_READ | VM_WRITE;
    t=back_with_physical((uint64_t*)(vma->vm_start)); // allocating this address now itself
    if(!t) return -1;*/
    
    vma = alloc_vma(process_task_struct->mm); // a vma for the stack
    vma->vm_mm = process_task_struct->mm;
    vma->vm_start = (uint64_t)(process_task_struct->stack);
    vma->vm_end = (uint64_t)(process_task_struct->stack)+process_task_struct->ulimit;
    //vma->vm_end = (uint64_t)(process_task_struct->stack)+4096;
    vma->vm_memsz = process_task_struct->ulimit;
    vma->vm_pgoff = 0;
    vma->vm_next = NULL;
    vma->vm_filestart = NULL;
    vma->vm_flags = 0 | VM_READ | VM_WRITE | VM_GROWSDOWN;
    
    //printf("DEBUG: process_task_struct: kernel_stack=%p kernel_rsp=%p stack=%p pid=%d rsp=%p cr3=%p\n",process_task_struct->kernel_stack,process_task_struct->kernel_rsp,process_task_struct->stack,process_task_struct->pid,process_task_struct->rsp,process_task_struct->cr3);
    //printf("PROCESS_SPACE_DEBUG: process_task_struct: %p\n",process_task_struct);
    
    // For STDIN
    process_task_struct->last_read=process_task_struct->last_write=-1;
    process_task_struct->last_eread=process_task_struct->last_ewrite=-1;
    // Switch back to Kernel's virtual address space
    asm volatile("movq %0, %%cr3":: "b"(temp_cr3));
    //asm volatile( "sti");
    //current=1; // this will be removed later. Currently kept just for testing
    process_count++;
    run_queue[process_count]=proc_id;
    return proc_id;
}
int create_process(char* zddr,char* proc_name,char** argv,char** envp) // creates a process and returns pid
{
    struct task_struct* process_task_struct;
    uint32_t proc_id = 0,i,k;
    uint32_t t;
    uint64_t temp_cr3;
    //uint64_t* addr;
    //printf("ProcNAME: %s\n",proc_name);
    /*printf("ARGV:-\n");
    for(i=0;argv[i]!=NULL;i++) printf("%s\n",argv[i]);
    printf("ENVP:-\n");
    for(i=0;envp[i]!=NULL;i++) printf("%s\n",envp[i]);*/
    process_task_struct = (struct task_struct*)pcb_malloc(200); //  1 page for pcb
    
    alloc_mm_struct(process_task_struct); // mm struct shares the same page as pcb
    process_task_struct->mm->mmap=NULL;
    process_task_struct->mm->map_count=0;
    
    if(!process_task_struct) return -1; // out of memory
    
    uint64_t* pml4 = pcb_malloc(200); // just need 1 page for the pml4 table of the new process
    if(!pml4) return -1; //out of memory
    
    pml4[511] = *(getPML4E((uint64_t*)(0xffffffff80200000))); // making the last entry point to the PDPE for kernel
    pml4[500]=ALIGN_DOWN((uint64_t)(*getPTE(pml4))) | PT_PRESENT_FLAG | PT_WRITABLE_FLAG; // self reference
    
    for(i=0;i<MAX_PROCESS_COUNT;i++) if(process_queue[i]==0) break;
    if(i==MAX_PROCESS_COUNT) return -1; // too many processes
    
    proc_id=i;
    process_task_struct->pid = proc_id;
    for(i=0;proc_name[i]!=0;i++) process_task_struct->procName[i]=proc_name[i];
    process_task_struct->procName[i]=0;
    process_task_struct->pml4_vaddr = (uint64_t)pml4;
    process_queue[proc_id]=(uint64_t)process_task_struct;
    
    process_task_struct->cr3 = ALIGN_DOWN((uint64_t)(*getPTE(pml4)));
    
    process_task_struct->is_zombie=0;
    process_task_struct->exit_status=0; // this field is ignored until the process exits
    
    process_task_struct->ulimit=10240; // stack size. Defaults to 10KB
    
    temp_cr3 = kernel_task_struct->cr3;
    //printf("Switching CR3\n");
    // Switch to process virtual address space
    //asm volatile( "cli");
    asm volatile("movq %0, %%cr3":: "b"(process_task_struct->cr3));
    //asm volatile( "sti");
    //printf("DEBUG: Virtual Address Space successfully switched\n");
    process_task_struct->parent = 0;
    process_task_struct->stack = (uint64_t*)(0xffffff8000000000-process_task_struct->ulimit); // allocating stack in higher most possible memory
    //process_task_struct->stack = (uint64_t*)(0xffffff8000000000)-512;
    //printf("Stack: %p\n",process_task_struct->stack);
    process_task_struct->kernel_stack = pcb_malloc(4097); // we need 2 pages
    process_task_struct->kernel_rsp = &process_task_struct->kernel_stack[1023];  // will be changed
       
    process_task_struct->kernel_stack[1023] = process_task_struct->kernel_stack[1022] = process_task_struct->kernel_stack[1021] = process_task_struct->kernel_stack[1020] = process_task_struct->kernel_stack[1019] = 0;
    process_task_struct->kernel_stack[1022] = (uint64_t)(0x23); // ss should be 0x23
    process_task_struct->kernel_stack[1021] = (uint64_t)(process_task_struct->stack)+process_task_struct->ulimit-1; // the user stack
    //process_task_struct->kernel_stack[1021] = (uint64_t)(&process_task_struct->stack[511]);
    //printf("USER_STACK_BOTTOM: %p\n",process_task_struct->kernel_stack[1021]);
    //process_task_struct->stack[509] = stack2;
    process_task_struct->kernel_stack[1020] = 0x00200200;
    process_task_struct->kernel_stack[1019] = (uint64_t)(0x1b); //cs should be 0x1b
    //process_task_struct->stack[509] = 0x200046;
    // process_task_struct->stack[506] the entry point will be defined here
    process_task_struct->kernel_stack[1017] = 0; //rax
    process_task_struct->kernel_stack[1016] = 0; //rbx
    process_task_struct->kernel_stack[1015] = 0; //rcx
    process_task_struct->kernel_stack[1014] = 0; //rdx -- third argument
    process_task_struct->kernel_stack[1013] = 0; //rsi -- second argument
    process_task_struct->kernel_stack[1012] = 0; //rdi -- first argument
    process_task_struct->kernel_stack[1011] = 0; //rbp
    process_task_struct->kernel_stack[1010] = 0; //r8
    process_task_struct->kernel_stack[1009] = 0; //r9
    process_task_struct->kernel_stack[1008] = 0; //r10
    process_task_struct->kernel_stack[1007] = 0; //r11
    process_task_struct->kernel_stack[1006] = 0; //r12
    process_task_struct->kernel_stack[1005] = 0; //r13
    process_task_struct->kernel_stack[1004] = 0; //r14
    process_task_struct->kernel_stack[1003] = 0; //r15
    process_task_struct->kernel_stack[1002] = (uint64_t)(_isr_032+40);
    process_task_struct->kernel_stack[1001] = 0; // extra rbp pop
    process_task_struct->kernel_rsp = &(process_task_struct->kernel_stack[1001]);
    
    char* zddr1 = zddr;
    struct elf_header* elf = (struct elf_header*)zddr;
    uint16_t phnum = elf->phnum,j;
    uint16_t phsize = elf->phentsize;
    uint64_t phoff = elf->phoff;
    //process_task_struct->stack[506] = elf->entry;
    process_task_struct->kernel_stack[1018] = elf->entry;
    zddr+=phoff;
    //uint64_t k;
    uint64_t brk_begin=0x5000; // just any initial value
    //printf("elf->entry = %p\n",elf->entry);
    //printf("[SYSTEM_MESSAGE]Creating VMAs\n");
    for(j=0;j<phnum;j++) {
                          struct prog_header* pheader = (struct prog_header*)zddr;
                          uint32_t type = pheader->p_type;
                          uint32_t flags = pheader->p_flags;
                          uint64_t vaddr = pheader->p_vaddr;
                          uint64_t offset = pheader->p_offset;
                          uint64_t size = pheader->p_memsz;
                          //printf("type=%p vaddr=%p offset=%p size=%p\n",type,vaddr,offset,size);
                          if(type==PT_LOAD) // check for LOAD type
                          {
                           //printf("[SYSTEM_MESSAGE]type=%p vaddr=%p offset=%p size=%p\n",type,vaddr,offset,size);
                           struct vm_area_struct* vma = alloc_vma(process_task_struct->mm);
                           vma->vm_mm=process_task_struct->mm;
                           vma->vm_start = vaddr;
                           vma->vm_end = (uint64_t)((char*)vaddr+size);
                           brk_begin = vma->vm_end;
                           vma->vm_memsz = size;
                           vma->vm_pgoff = offset;
                           vma->vm_next = NULL;
                           vma->vm_filestart = (uint64_t)zddr1;
                           vma->vm_flags=(uint64_t)flags;
                          }
                          zddr+=phsize;
                         }
    
    struct vm_area_struct* vma;
    
    vma = alloc_vma(process_task_struct->mm); // a vma for storing argv and envp
    vma->vm_mm = process_task_struct->mm;
    vma->vm_start = ALIGN_DOWN(brk_begin+0x1000);
    vma->vm_end = ALIGN_DOWN(brk_begin+0x2000);
    vma->vm_memsz = 0;
    vma->vm_pgoff = 0;
    vma->vm_next = NULL;
    vma->vm_filestart = NULL;
    vma->vm_flags = 0 | VM_READ | VM_WRITE | VM_GROWSDOWN;
    //printf("NO PF TILL NOW\n");
    t=back_with_physical((uint64_t*)(vma->vm_start)); // allocating this address now itself
    
    if(!t) return -1;
    volatile uint64_t* pml4e = getPML4E((uint64_t*)(vma->vm_start));
    volatile uint64_t* pdpe = getPDPE((uint64_t*)(vma->vm_start));
    volatile uint64_t* pde = getPDE((uint64_t*)(vma->vm_start));
    volatile uint64_t* pte = getPTE((uint64_t*)(vma->vm_start));
    (*pte) = (*pte) | PT_USER_FLAG;
    (*pde) = (*pde) | PT_USER_FLAG;
    (*pdpe) = (*pdpe) | PT_USER_FLAG;
    (*pml4e) = (*pml4e) | PT_USER_FLAG;
    char* cla_addr=(char*)vma->vm_start;
    process_task_struct->kernel_stack[1012] = (uint64_t)cla_addr; // first argument goes to rdi -- addr of start of argv
    for(i=0;argv[i]!=NULL;i++) {
                                //printf("<i=%d>",i);
                                for(k=0;argv[i][k]!=0;k++) {
                                                            *cla_addr=argv[i][k];
                                                            //printf("%p %c ",cla_addr,*cla_addr);
                                                            cla_addr++;
                                                           }
                                *cla_addr='\0';
                                cla_addr++;
                               }
    process_task_struct->kernel_stack[1013] = (uint64_t)cla_addr; // second argument goes to rsi -- addr of start of envp
    for(i=0;envp[i]!=NULL;i++) {
                                for(k=0;envp[i][k]!=0;k++) {
                                                            *cla_addr=envp[i][k];
                                                            //printf("%p %c ",cla_addr,*cla_addr);
                                                            cla_addr++;
                                                           }
                                *cla_addr='\0';
                                cla_addr++;
                               }
    process_task_struct->kernel_stack[1014] = (uint64_t)cla_addr; // third argument goes to rdx -- addr of end of envp
    
    vma = alloc_vma(process_task_struct->mm); // a vma for heap1
    vma->vm_mm = process_task_struct->mm;
    vma->vm_start = ALIGN_DOWN(brk_begin+0x2000);
    vma->vm_end = ALIGN_DOWN(brk_begin+0x2000);
    vma->vm_memsz = 0;
    vma->vm_pgoff = 0;
    vma->vm_next = NULL;
    vma->vm_filestart = NULL;
    vma->vm_flags = 0 | VM_READ | VM_WRITE;
    
    /*vma = alloc_vma(process_task_struct->mm); // an area for passing arguments
    vma->vm_mm = process_task_struct->mm;
    vma->vm_start = 0x0000fa8000000000; // next address after 500th PML4 entry
    vma->vm_end = 0x0000fa8000000000+0x1000;
    vma->vm_memsz = 0;
    vma->vm_pgoff = 0;
    vma->vm_next = NULL;
    vma->vm_filestart = NULL;
    vma->vm_flags = 0 | VM_READ | VM_WRITE;
    t=back_with_physical((uint64_t*)(vma->vm_start)); // allocating this address now itself
    if(!t) return -1;*/
    
    vma = alloc_vma(process_task_struct->mm); // a vma for the stack
    vma->vm_mm = process_task_struct->mm;
    vma->vm_start = (uint64_t)(process_task_struct->stack);
    vma->vm_end = (uint64_t)(process_task_struct->stack)+process_task_struct->ulimit;
    //vma->vm_end = (uint64_t)(process_task_struct->stack)+4096;
    vma->vm_memsz = process_task_struct->ulimit;
    vma->vm_pgoff = 0;
    vma->vm_next = NULL;
    vma->vm_filestart = NULL;
    vma->vm_flags = 0 | VM_READ | VM_WRITE | VM_GROWSDOWN;
    
    //printf("DEBUG: process_task_struct: kernel_stack=%p kernel_rsp=%p stack=%p pid=%d rsp=%p cr3=%p\n",process_task_struct->kernel_stack,process_task_struct->kernel_rsp,process_task_struct->stack,process_task_struct->pid,process_task_struct->rsp,process_task_struct->cr3);
    //printf("PROCESS_SPACE_DEBUG: process_task_struct: %p\n",process_task_struct);
    
    // For STDIN
    process_task_struct->last_read=process_task_struct->last_write=-1;
    process_task_struct->last_eread=process_task_struct->last_ewrite=-1;
    // Switch back to Kernel's virtual address space
    asm volatile("movq %0, %%cr3":: "b"(temp_cr3));
    
    //asm volatile( "sti");
    //current=1; // this will be removed later. Currently kept just for testing
    process_count++;
    //printf("NO PF TILL NOW\n");
    run_queue[process_count]=proc_id;
    //printf("NO PF TILL NOW\n");
    return proc_id;
}

void createForkedProcess()
{
    struct task_struct* parent_task_struct = getCurrentTaskStruct();
    struct task_struct* process_task_struct;
    uint32_t proc_id = 0,i,m;
    //uint64_t temp_cr3;
    
    process_task_struct = (struct task_struct*)pcb_malloc(200); //  1 page for pcb
    alloc_mm_struct(process_task_struct); // mm struct shares the same page as pcb
    process_task_struct->mm->mmap=NULL;
    process_task_struct->mm->map_count=0;
    if(!process_task_struct) {forked_cid=-1;return;} // out of memory
    
    process_task_struct->parent = parent_task_struct->pid;
    uint64_t* pml4 = pcb_malloc(200); // just need 1 page for the pml4 table of the new process
    if(!pml4) {forked_cid=-1;process_task_struct->is_zombie=1;return;} //out of memory
    
    pml4[511] = *(getPML4E((uint64_t*)(0xffffffff80200000))); // making the last entry point to the PDPE for kernel
    pml4[500]=ALIGN_DOWN((uint64_t)(*getPTE(pml4))) | PT_PRESENT_FLAG | PT_WRITABLE_FLAG; // self reference
    
    for(i=0;i<MAX_PROCESS_COUNT;i++) if(process_queue[i]==0) break;
    if(i==MAX_PROCESS_COUNT) {forked_cid=-1;process_task_struct->is_zombie=1;return;} // too many processes
    
    proc_id=i;
    process_task_struct->pid = proc_id;
    process_task_struct->pml4_vaddr = (uint64_t)pml4;
    process_queue[proc_id]=(uint64_t)process_task_struct;
    
    process_task_struct->cr3 = ALIGN_DOWN((uint64_t)(*getPTE(pml4)));
    
    process_task_struct->is_zombie=0;
    process_task_struct->exit_status=0; // this field is ignored until the process exits
    
    process_task_struct->ulimit = parent_task_struct->ulimit;
    //temp_cr3 = parent_task_struct->cr3;
    process_task_struct->stack = (uint64_t*)(0xffffff8000000000-process_task_struct->ulimit);
    //printf("Stack: %p\n",process_task_struct->stack);
    
    // Create child process' kernel stack
    process_task_struct->kernel_stack = pcb_malloc(4097); // we need 2 pages
    process_task_struct->kernel_rsp = &process_task_struct->kernel_stack[1023];  // will be changed
    process_task_struct->kernel_stack[1023] = process_task_struct->kernel_stack[1022] = process_task_struct->kernel_stack[1021] = process_task_struct->kernel_stack[1020] = process_task_struct->kernel_stack[1019] = 0;
    process_task_struct->kernel_stack[1022] = (uint64_t)(0x23); // ss should be 0x23
    process_task_struct->kernel_stack[1021] = return_stack_addr; // the return stack address (same as parent's)
    process_task_struct->kernel_stack[1020] = 0x00200200;
    process_task_struct->kernel_stack[1019] = (uint64_t)(0x1b); //cs should be 0x1b
    process_task_struct->kernel_stack[1018] = return_address; // entry point same as return address of parent
    process_task_struct->kernel_stack[1017] = 0; //rax
    process_task_struct->kernel_stack[1016] = rbx; //rbx
    process_task_struct->kernel_stack[1015] = rcx; //rcx
    process_task_struct->kernel_stack[1014] = rdx; //rdx
    process_task_struct->kernel_stack[1013] = rsi; //rsi
    process_task_struct->kernel_stack[1012] = rdi; //rdi
    process_task_struct->kernel_stack[1011] = rbp; //rbp
    process_task_struct->kernel_stack[1010] = r8; //r8
    process_task_struct->kernel_stack[1009] = r9; //r9
    process_task_struct->kernel_stack[1008] = r10; //r10
    process_task_struct->kernel_stack[1007] = r11; //r11
    process_task_struct->kernel_stack[1006] = r12; //r12
    process_task_struct->kernel_stack[1005] = r13; //r13
    process_task_struct->kernel_stack[1004] = r14; //r14
    process_task_struct->kernel_stack[1003] = r15; //r15
    process_task_struct->kernel_stack[1002] = (uint64_t)(_isr_032+40);
    process_task_struct->kernel_stack[1001] = 0; // extra rbp pop
    process_task_struct->kernel_rsp = &(process_task_struct->kernel_stack[1001]);
    
    //copy VMAs
    struct vm_area_struct* vma_parent = parent_task_struct->mm->mmap;
    struct vm_area_struct* vma_child;
    //struct vm_area_struct* vma;
    while(vma_parent!=NULL) {
                             vma_child = alloc_vma(process_task_struct->mm);
                             vma_child->vm_end=vma_parent->vm_end;
                             vma_child->vm_filestart=vma_parent->vm_filestart;
                             vma_child->vm_flags=vma_parent->vm_flags;
                             vma_child->vm_memsz=vma_parent->vm_memsz;
                             vma_child->vm_mm=process_task_struct->mm;
                             vma_child->vm_next=NULL;
                             vma_child->vm_pgoff=vma_parent->vm_pgoff;
                             vma_child->vm_start=vma_parent->vm_start;
                             vma_parent=vma_parent->vm_next;
                            }
    
    // print VMA's to verify
    vma_parent = parent_task_struct->mm->mmap;
    vma_child = process_task_struct->mm->mmap;
    while(vma_parent!=NULL) {
                             /*vma=vma_parent;
                             printf("VMA Parent Contents:\n");
                             printf("SIZE: %p ADDR: %p START: %p END: %p NEXT: %p FLAGS %p PGOFF: %p MMSZ: %p FILE_START: %p\n",sizeof(struct vm_area_struct),vma,vma->vm_start,vma->vm_end,vma->vm_next,vma->vm_flags,vma->vm_pgoff,vma->vm_memsz,vma->vm_filestart);
                             vma=vma_child;
                             printf("VMA Child Contents:\n");
                             printf("SIZE: %p ADDR: %p START: %p END: %p NEXT: %p FLAGS %p PGOFF: %p MMSZ: %p FILE_START: %p\n",sizeof(struct vm_area_struct),vma,vma->vm_start,vma->vm_end,vma->vm_next,vma->vm_flags,vma->vm_pgoff,vma->vm_memsz,vma->vm_filestart);
                             */
                             uint64_t start = vma_parent->vm_start;
                             uint64_t end = vma_parent->vm_end;
                             uint64_t sp = ALIGN_DOWN(start);
                             uint64_t ep = ALIGN_DOWN(end);
                             
                             //printf("%p %p\n",sp,ep);
                             uint64_t i;
                             for(i=sp;i<=ep;i+=0x1000) {
                                                     //printf("COW fork: Copying virt page: %p\n",i);
                                                     volatile uint64_t* pml4e = getPML4E((uint64_t*)i);
                                                     if(*pml4e & PT_PRESENT_FLAG) {
                                                               volatile uint64_t* pdpe = getPDPE((uint64_t*)i);
                                                               if(*pdpe & PT_PRESENT_FLAG) {
                                                                        volatile uint64_t* pde = getPDE((uint64_t*)i);
                                                                        if(*pde & PT_PRESENT_FLAG) {
                                                                                volatile uint64_t* pte = getPTE((uint64_t*)i);
                                                                                if(*pte & PT_PRESENT_FLAG) {
                                                                                    //printf("COW FORK: PML4E=%p PDPE=%p PDE=%p PTE=%p\n",*pml4e,*pdpe,*pde,*pte);
                                                                                    uint64_t physpage = ALIGN_DOWN(*pte);
                                                                                    int page_number = (int)(physpage/4096);
                                                                                    int pgrefcnt=get_ref_cnt(page_number);
                                                                                    if(pgrefcnt>=PER_PROCESS_LIMIT) {
                                                                                                                     forked_cid=-1;process_task_struct->is_zombie=1;return;
                                                                                                                    }
                                                                                    mm_inc_ref_cnt(page_number); // increase reference count
                                                                                    //printf("Ref count of %p,%p increased to %d\n",i,physpage,get_ref_cnt(page_number));
                                                                                     //uint64_t* vp = (uint64_t*)i;
                                                                                     //int j;
                                                                                     //for(j=0;j<10;j++) printf("vp[%d]=%p ",j,vp[j]);
                                                                                    *pte |= PT_COW; // set COW bit
                                                                                    *pte ^= PT_WRITABLE_FLAG; // unset writable bit (make it read only)
                                                                                    *pde |= PT_COW; // set COW bit
                                                                                    *pde ^= PT_WRITABLE_FLAG;
                                                                                    *pdpe |= PT_COW; // set COW bit
                                                                                    *pdpe ^= PT_WRITABLE_FLAG;
                                                                                    *pml4e |= PT_COW; // set COW bit
                                                                                    *pml4e ^= PT_WRITABLE_FLAG;
                                                                                    // Duplicate Page Tables in child process
                                                                                    asm volatile("movq %0, %%cr3":: "b"(process_task_struct->cr3));
                                                                                    volatile uint64_t* cpte = getPTE((uint64_t*)i); // takes care of forming page tables
                                                                                    volatile uint64_t* cpde = getPDE((uint64_t*)i);
                                                                                    volatile uint64_t* cpdpe = getPDPE((uint64_t*)i);
                                                                                    volatile uint64_t* cpml4e = getPML4E((uint64_t*)i);
                                                                                    *cpde |= PT_COW | PT_USER_FLAG; // set COW bit
                                                                                    *cpde ^= PT_WRITABLE_FLAG;
                                                                                    *cpdpe |= PT_COW | PT_USER_FLAG; // set COW bit
                                                                                    *cpdpe ^= PT_WRITABLE_FLAG;
                                                                                    *cpml4e |= PT_COW | PT_USER_FLAG; // set COW bit
                                                                                    *cpml4e ^= PT_WRITABLE_FLAG;
                                                                                    *cpte = physpage | PT_USER_FLAG | PT_COW | PT_PRESENT_FLAG | PT_TERMINAL_FLAG;
                                                                                    asm volatile("movq %0, %%cr3":: "b"(parent_task_struct->cr3));
                                                                                }
                                                                        }
                                                               }
                                                     } 
                                                    }
                             vma_parent=vma_parent->vm_next;
                             vma_child=vma_child->vm_next;
                            }
    for(m=0;parent_task_struct->procName[m]!=0;m++) process_task_struct->procName[m]=parent_task_struct->procName[m];
    process_task_struct->procName[m]=0;
    process_task_struct->last_read=process_task_struct->last_write=-1;
    process_count++;
    run_queue[process_count]=proc_id;
    //printf("[SYSTEM_MESSAGE]FORK: RETURN ADDR: %p RETURN_STACK: %p rax=%p rbx=%p rcx=%p rdx=%p rsi=%p rdi=%p r8=%p r9=%p r10=%p r11=%p r12=%p r13=%p r14=%p r15=%p\n",return_address,return_stack_addr,0,rbx,rcx,rdx,rsi,rdi,r8,r9,r10,r11,r12,r13,r14,r15);
    forked_cid=proc_id;
    //forked_cid=7;
}
int getFirstZombieChild()
{
    int i;
    for(i=1;i<MAX_PROCESS_COUNT;i++) {
                                      if(!process_queue[i]) continue;
                                      struct task_struct* CPCB = (struct task_struct*)process_queue[i];
                                      if(CPCB->parent==run_queue[current] && CPCB->is_zombie) {
                                                               waitpit_ret_exit_st = (int)CPCB->exit_status;
                                                               pcb_free((uint64_t*)(CPCB),488); //just free 1 page
                                                               process_queue[i]=0;
                                                               return i;
                                      }
                                     }
    return -1;
}
int isZombieChild()
{
    int i = waiting_on[run_queue[current]];
    struct task_struct* CPCB = (struct task_struct*)process_queue[i];
    if(CPCB->is_zombie) {
                               waitpit_ret_exit_st = (int)CPCB->exit_status;
                               pcb_free((uint64_t*)(CPCB),488); //just free 1 page
                               process_queue[i]=0;
                               return 1;
    }
    return 0;
}
void update_prev_next()
{
    prev=current;
    while(1) {
              current = (current+1)%(process_count+1);
              if(prev==0 && current==0) break;
              if(current==0) current++;
              if(!sleep_time[run_queue[current]] && !waiting_on[run_queue[current]] && !waiting_to_read[run_queue[current]]) break;
              if(waiting_on[run_queue[current]]==-1) {
                                                      //printf("[SYSTEM_MESSAGE]Process %d in wait state\n",run_queue[current]);
                                                      zchild = getFirstZombieChild();
                                                      if(zchild!=-1) {
                                                                      //printf("[SYSTEM_MESSAGE]Process %d has zombie child %d. Will switch now.\n",run_queue[current],zchild);
                                                                      //asm("hlt");
                                                                      waitpid_ret_pid=zchild;
                                                                      
                                                                      waiting_on[run_queue[current]]=0;
                                                                      break;
                                                                     }
                                                     }
              else if(waiting_on[run_queue[current]]>0){
                                                        if(isZombieChild()) {
                                                        //printf("[SYSTEM_MESSAGE]Process %d has zombie child %d. Will switch now.\n",run_queue[current],waiting_on[run_queue[current]]);
                                                        
                                                        waitpid_ret_pid=waiting_on[run_queue[current]];
                                                        waiting_on[run_queue[current]]=0;
                                                        break;
                                                        }
                                                       }
              else if(waiting_to_read[run_queue[current]]) {
                                                            struct task_struct* PCB = (struct task_struct*)process_queue[run_queue[current]];
                                                            //printf("[SYSTEM_MESSAGE]Process: %d last_read=%d, last_write=%d\n",run_queue[current],PCB->last_read,PCB->last_write);
                                                            if(PCB->last_read<PCB->last_write) {waiting_to_read[run_queue[current]]=0;break;}
                                                           }
              if(current==prev) {current=0;break;}
             }
    /*current = (current+1)%(process_count+1);
    if(current==0) current++;
    while(sleep_time[run_queue[current]]>1) {
                                             //printf("Process %d will sleep for %d more seconds. Skipping it.\n",run_queue[current],sleep_time[run_queue[current]]);
                                             current = (current+1)%(process_count+1);
                                             if(prev==0 && current==0) 
                                             {
                                              //printf("Jumping to main()\n");
                                              break;
                                             }
                                             if(current==0) current++;
                                             if(current==prev) break;
                                            }
    
    if(sleep_time[run_queue[current]]==1) {
                                           printf("Time to go back to Process %d\n",run_queue[current]);
                                           sleep_time[run_queue[current]] = 0;
                                           //asm volatile("hlt");
                                          }*/
    //printf("Switching to process %d\n",run_queue[current]);
    next = current;
}

void get_exit_status(uint64_t c1)
{
    //printf("Received: Exit Status: %p\n",c1);
    exit_st=(char)c1;
}

void exit_current_process()
{
    asm volatile(                                                   
    		"movq %0, %%rsp;"                                           
    		:                                                           
    		:"r"(((struct task_struct*)process_queue[0])->kernel_rsp)       
   	);
    
    //int current1 = run_queue[current];
    struct task_struct* PCB = (struct task_struct*)process_queue[run_queue[current]];
    struct task_struct* ParentPCB = (struct task_struct*)process_queue[PCB->parent];
    struct vm_area_struct* first_vma = PCB->mm->mmap,*vma;
    vma = first_vma;
    // Change to Kernel RSP here
    
    // Free user space memory using the VMAs
    while(vma!=NULL) {
                      uint64_t start = vma->vm_start;
                      uint64_t end = vma->vm_end;
                      end--;
                      uint64_t sp = ALIGN_DOWN(start);
                      uint64_t ep = ALIGN_DOWN(end);
                      //printf("%d %p %p--",PCB->mm->map_count,sp,ep);
                      //printf("%p %p\n",sp,ep);
                      uint64_t i;
                      for(i=sp;i<=ep;i+=0x1000) {
                                              free_page((uint64_t*)i);
                                              //printf("DEBUG: exit_current_process: page for %p freed.\n",i);
                                             }
                      vma=vma->vm_next;
                     }
    // Change to Kernel cr3
    asm volatile("movq %0, %%cr3":: "b"(((struct task_struct*)process_queue[0])->cr3));
    // Free Kernel Stack for the process
    pcb_free((uint64_t*)(PCB->kernel_stack),4098); // Free 2 pages
    // Free PML4 table
    pcb_free((uint64_t*)(PCB->pml4_vaddr),1); // Free one page
    // Free PCB
    //pcb_free((uint64_t*)(PCB),1); // Free one page
    
    // Mark the process as a zombie and set its exit status
    PCB->is_zombie=1;
    PCB->exit_status=exit_st;
    // Check if its parent is alive. If not, change its parent to 1
    if(PCB->parent==0 || ParentPCB==NULL || ParentPCB->is_zombie==1) {
                      //printf("[SYSTEM_MESSAGE]Zombie Process %d inherited by 1\n",current1);
                      PCB->parent=1;
    } 
    else {
          //printf("[SYSTEM_MESSAGE]Zombie Process %d has a parent %d\n",current1,PCB->parent);
         }
    // Remove from queue => update all PIDs
    process_count--;
    if(process_count!=0) {
                          while(current<=process_count) {
                                                         run_queue[current]=run_queue[current+1];
                                                         current++;
                                                        }
                         }
    current=0;
    //int j;
    //for(j=0;j<=3;j++) printf("%p ",process_queue[j]);
    //printf("\n");
    //printf("[SYSTEM_MESSAGE]Process %d exited with status: %d\n",current1,PCB->exit_status);
    //printf("RUN QUEUE CONTENTS: ");
    //int i;
    //for(i=0;i<=process_count;i++) printf("%d ",run_queue[i]);
    //printf("\n\n");
    
    // jump to kernel here
    asm volatile("pop %rbx"); // This was pushed in by schedule
    asm volatile("pop %rax"); // Return address in _isr_032
    asm volatile("jmp *%rax");
    
}

void schedule()
{
    static int counter = 0;
    counter++;
    if(process_count && counter==2) {
        update_prev_next();
        asm volatile(                                                   
    	"movq %%rsp, %0;"                                               
    	:"=g"(((struct task_struct*)process_queue[run_queue[prev]])->kernel_rsp)          
        );                                                              
    	
        asm volatile("movq %0, %%cr3":: "b"(((struct task_struct*)process_queue[run_queue[next]])->cr3));
        
        asm volatile(                                                   
    		"movq %0, %%rsp;"                                           
    		:                                                           
    		:"r"(((struct task_struct*)process_queue[run_queue[next]])->kernel_rsp)       
    	);      
        tss.rsp0 =  (uint64_t)(&(((struct task_struct*)process_queue[run_queue[next]])->kernel_stack[1023]));
        //setup_tss();     
        counter=0;
        
        PIC_sendEOI(0);
        
    }
}
void schedule2()
{
    update_prev_next();
    asm volatile(                                                   
	"movq %%rsp, %0;"                                               
	:"=g"(((struct task_struct*)process_queue[run_queue[prev]])->kernel_rsp)          
    );                                                              
	
    asm volatile("movq %0, %%cr3":: "b"(((struct task_struct*)process_queue[run_queue[next]])->cr3));
    
    asm volatile(                                                   
		"movq %0, %%rsp;"                                           
		:                                                           
		:"r"(((struct task_struct*)process_queue[run_queue[next]])->kernel_rsp)       
	);      
    tss.rsp0 =  (uint64_t)(&(((struct task_struct*)process_queue[run_queue[next]])->kernel_stack[1023]));
    //setup_tss();     
    //counter=0;
}
struct task_struct* getCurrentTaskStruct()
{
    //printf("current = %d proc_ID = %d PCB = %p\n",current,run_queue[current],process_queue[run_queue[current]]);
    return ((struct task_struct*)process_queue[run_queue[current]]);
}

int getCurrentPID()
{
    return run_queue[current];
}
uint32_t forkCurrentProcess()
{
    //int i;
    uint64_t* rsp1;
    asm volatile("movq %%rsp, %0;" : "=g"(return_address));
    rsp1 = (uint64_t*)return_address;
    //for(i=0;i<=30;i++) printf("%p ",rsp1[i]);
    //printf("\n");
    return_address = rsp1[18]; // By hit and trial
    return_stack_addr=rsp1[21]; // rsp
    rbx = rsp1[17];
    rcx = rsp1[16];
    rdx = rsp1[15];
    rsi = rsp1[14];
    rdi = rsp1[13];
    rbp = rsp1[12];
    r8 = rsp1[11];
    r9 = rsp1[10];
    r10 = rsp1[9];
    r11 = rsp1[8];
    r12 = rsp1[7];
    r13 = rsp1[6];
    r14 = rsp1[5];
    r15 = rsp1[4];
    createForkedProcess();
    //printf("FORK: RETURN ADDR: %p\n",return_address);
    return forked_cid;
}
uint64_t getLocation(char* prog)
{
    int size = sizeof(struct posix_header_ustar),size2;
    char * zddr;
    zddr = &_binary_tarfs_start;
    struct posix_header_ustar* star;
    while(1)
    {
     star = (struct posix_header_ustar*)zddr;
     size2 = otod(atoi(star->size));
     if(size2%512) size2=((size2+512)/512)*512;
     //if(!equal(star->name,"")) printf("%s %d %d\n",star->name,size,size2);
     //if(equal(star->name,"bin/hello3") || equal(star->name,"bin/hello3") || equal(star->name,"bin/hello4")) create_process(zddr+size);
     if(size2>0 && equal(star->name,prog)) {
                if(!((*(zddr+size+1) == 'E')&&(*(zddr+size+1) == 'L')&&(*(zddr+size+1) == 'F'))) {
                                   //printf("FOUND: %s. File is executable\n",star->name);
                                   return (uint64_t)(zddr+size);
                }
                else {
                     printf("FOUND: %s. But file is not executable\n",star->name);
                     return 0;
                }
     }
     zddr+=size+size2;
     if(zddr>&_binary_tarfs_end) break;
    }
    return 0;
}
void updateChecklist()
{
     int i=0,j=0,k,t,m,n;
     //checkList[i][0]='';checkList[i][1]=0;
     struct task_struct* PCB = (struct task_struct*)process_queue[run_queue[current]];
     for(k=0;PCB->procName[k]!=0;k++) if(PCB->procName[k]=='/') j=k;
     for(k=0;k<=j;k++) checkList[i][k]=PCB->procName[k];
     for(j=0;kfn[j]!=0;j++) {checkList[i][k]=kfn[j];k++;}
     checkList[i][k]=0;i++;
     for(j=0;kenvpaddr1[j][0]!=0;j++) if(kenvpaddr1[j][0]=='P' && kenvpaddr1[j][1]=='A' && kenvpaddr1[j][2]=='T' && kenvpaddr1[j][3]=='H' && kenvpaddr1[j][4]=='=') break;
     t=5;
     for(k=0;kenvpaddr1[j][k]!=0;k++) {
                                       if(kenvpaddr1[j][k]==';') {
                                                                  m=0;
                                                                  for(;t<k;t++) {
                                                                                 checkList[i][m]=kenvpaddr1[j][t];
                                                                                 m++;
                                                                                }
                                                                  t=k+1;
                                                                  checkList[i][m]='/';m++;
                                                                  for(n=0;kfn[n]!=0;n++) {checkList[i][m]=kfn[n];m++;}
                                                                  checkList[i][m]=0;
                                                                  i++;
                                                                 }
                                      }
     m=0;
     for(;t<k;t++) {
                    checkList[i][m]=kenvpaddr1[j][t];
                    m++;
                   }
     t=k+1;
     checkList[i][m]='/';m++;
     for(n=0;kfn[n]!=0;n++) {checkList[i][m]=kfn[n];m++;}
     checkList[i][m]=0;
     i++;
     //printf("UPDATE CHECKLIST: i=%d\n",i);
     //for(m=1;checkList[m][0]!=0;m++) printf("%s\n",checkList[m]);
     checkList[i][0]=0;
}
uint32_t execvpeCurrentProcess(uint64_t filaddr, uint64_t argvaddr, uint64_t envpaddr)
{
    // IMP: LOCAL VARIABLES SHOULD NOT BE USED AFTER STACK CHANGE
    int i,j,m;
    uint64_t loc=0;
    //printf("[SYSTEM_MESSAGE]DEBUG EXECVPE %p %p %p\n",filaddr,argvaddr,envpaddr);
    char* fn = (char*)filaddr;
    //printf("%s\n",fn);
    char** argvaddr1 = (char**)argvaddr;
    char** envpaddr1 = (char**)envpaddr;
    for(i=0;fn[i]!=0;i++) kfn[i]=fn[i];
    kfn[i]=0;
    kfn1 = kfn;
    //printf("%s\n",kfn);
    PCB = (struct task_struct*)process_queue[run_queue[current]];
    
    for(i=0;argvaddr1[i]!=NULL;i++) {
                                     for(j=0;argvaddr1[i][j]!=0;j++) {
                                                                      kargvaddr1[i][j]=argvaddr1[i][j];
                                                                     }
                                     kargvaddr1[i][j]=0;
                                     kargvaddr2[i]=&kargvaddr1[i][0];
                                    }
    kargvaddr1[i][0]=NULL;
    kargvaddr2[i]=NULL;
    for(i=0;envpaddr1[i]!=NULL;i++) {
                                     for(j=0;envpaddr1[i][j]!=0;j++) {
                                                                      kenvpaddr1[i][j]=envpaddr1[i][j];
                                                                     }
                                     kenvpaddr1[i][j]=0;
                                     kenvpaddr2[i]=&kenvpaddr1[i][0];
                                    }
    kenvpaddr1[i][0]=NULL;
    kenvpaddr2[i]=NULL;
    //for(i=0;kargvaddr2[i]!=NULL;i++) printf("%s\n",kargvaddr2[i]);
    //for(i=0;kenvpaddr2[i]!=NULL;i++) printf("%s\n",kenvpaddr2[i]);
    updateChecklist();
    //printf("CHECKLIST:- \n");
    for(m=0;checkList[m][0]!=0;m++) {
                                     loc=getLocation(checkList[m]);
                                     //printf("%s %p\n",checkList[m],loc);
                                     if(loc) {
                                              //printf("FOUND: %s\n",checkList[m]);
                                              kfn1=checkList[m];
                                              break;
                                             }
    }
    if(!loc) {
              loc=getLocation(fn);
              if(!loc) return -1;
    }

    int current1 = run_queue[current],temp_proc;

    temp_proc = create_process_exec(current1,PCB->parent,(char*)loc,kfn1,kargvaddr2,kenvpaddr2);
    //printf("GPF NOT YET %d\n",current1);
    if(temp_proc==-1) {
                      //printf("GPF NOT YET %d\n",current1);
                      exit_current_process();
    }
    //printf("Created Temporary Process: %d\n",temp_proc);
    // Exchanging positions in process_queue
    tempPCB = process_queue[current1];
    process_queue[current1]=process_queue[temp_proc];
    process_queue[temp_proc]=tempPCB;
    current1=temp_proc;
    PCB->pid=temp_proc;
    PCB->parent=0;
    //printf("Created Temporary Process: %d current=%d\n",temp_proc,current);
    for(i=0;i<=process_count;i++) {
                                   if(run_queue[i]==current1) current=i;
                                   //printf("%d ",run_queue[i]);
    }
    //printf("current=%d\n\n\n",current);
    //asm("hlt"); 
    exit_current_process();
    //asm("hlt");
    //printf("GPF NOT YET %d. Address Space Copied.\n",current1);
    //asm("hlt");
    //printf("GPF NOT YET %d\n",current1);
    
    //asm volatile("hlt");
    //printf("RUN QUEUE CONTENTS: ");
    //int i;
    //for(i=0;i<=process_count;i++) printf("%d ",run_queue[i]);
    //printf("\n\n");
    //current=0;
    // Change to Kernel cr3
    //asm volatile("movq %0, %%cr3":: "b"(((struct task_struct*)process_queue[0])->cr3));
    
    //asm volatile("pop %rbx"); // This was pushed in by schedule
    //asm volatile("pop %rax"); // Return address in _isr_032
    //asm volatile("jmp *%rax");
    
    return -1;
}
void saveCurrentStateAndJumpToMain()
{
    asm volatile(                                                   
    	"movq %%rsp, %0;"                                               
    	:"=g"(((struct task_struct*)process_queue[run_queue[current]])->kernel_rsp)          
        );
    // Change to Kernel RSP here
    current=0;
    asm volatile(                                                   
    		"movq %0, %%rsp;"                                           
    		:                                                           
    		:"r"(((struct task_struct*)process_queue[0])->kernel_rsp)       
   	);
   	// change to kernel cr3
   	asm volatile("movq %0, %%cr3":: "b"(((struct task_struct*)process_queue[0])->cr3));
    asm volatile("pop %rbx"); // This was pushed in by schedule
    asm volatile("pop %rax"); // Return address in _isr_032
    asm volatile("jmp *%rax");
}
uint32_t sleepCurrentProcess(uint32_t time)
{
    int currentPID = getCurrentPID();
    //printf("Process %d sleeps for %d seconds\n", currentPID, time);
    sleep_time[currentPID] = time;
    
    saveCurrentStateAndJumpToMain();
    
    return 0;
}
void getWaitPid(uint64_t pid,uint64_t status)
{
    uint32_t currentPID = getCurrentPID(),i;
    //uint32_t* st = (uint32_t*)status;
    uint64_t ret=0;
    //printf("WaitPid Called. Current Process: %d. Wait called on process: %d status to be stored in %p",currentPID,pid,status);
    if(pid==-1) {
                 //printf("[SYSTEM_MESSAGE]PID %d calls wait: %d %d\n",currentPID,pid);
                 for(i=1;i<MAX_PROCESS_COUNT;i++) {
                                                   //printf("[SYSTEM_MESSAGE]process_queue[%d]=%d\n",i,process_queue[i]);
                                                   if(!process_queue[i]) continue;
                                                   struct task_struct* child_task_struct = (struct task_struct*)process_queue[i];
                                                   if(child_task_struct->parent == currentPID) {
                                                      //printf("[SYSTEM_MESSAGE]Found Child: %d\n",child_task_struct->pid);
                                                      ret = child_task_struct->pid;
                                                      waiting_on[currentPID]=-1;
                                                      saveCurrentStateAndJumpToMain();
                                                      //yield();
                                                      return;
                                                      /*struct task_struct* ZCPCB = (struct task_struct*)process_queue[zchild];
                                                      
                                                      process_queue[zchild]=0;
                                                      
                                                      *st = ZCPCB->exit_status;
                                                      //printf("[SYSTEM_MESSAGE] ZCPID=%p ZCEXITST=%p\n",zchild,*st);
                                                      pcb_free((uint64_t*)(ZCPCB),1); // Free the child's PCB
                                                      //waiting_on[currentPID]=0;
                                                      
                                                      return zchild;*/
                                                   }
                 }
                 if(i==MAX_PROCESS_COUNT) {waitpid_ret_pid=-1;return;}
    }
    else if(pid>0) {
         if(pid>=MAX_PROCESS_COUNT) {waitpid_ret_pid=-1;return;}
         if(process_queue[pid]==0) {waitpid_ret_pid=-1;return;}
         struct task_struct* child_task_struct = (struct task_struct*)process_queue[pid];
         if(child_task_struct->parent == currentPID) {
                                                      //printf("PID>0 Found Child: %d\n",child_task_struct->pid);
                                                      ret = child_task_struct->pid;
                                                      waiting_on[currentPID]=(int)ret;
                                                      saveCurrentStateAndJumpToMain();
                                                      return;
         }
         else {waitpid_ret_pid=-1;return;}
    }
    else {
          {waitpid_ret_pid=-1;return;}
         }
}
void flushBuffer()
{
     int i=0,j=0,r,w;
     char* sbuf;
     struct task_struct* PCB = (struct task_struct*)process_queue[foregroundProcess];
     sbuf=PCB->stdin;
     r=PCB->last_read;
     w=PCB->last_write;
     if(r<=0) return;
     for(i=r;i<=w;i++) {
                        sbuf[j++]=sbuf[i];
                       }
     PCB->last_read=0;
     PCB->last_write=w-r;
     //printf("[SYSTEM_MESSAGE]Flushed Buffer: %d %d",PCB->last_read,PCB->last_write);
     
}
void flushErrBuffer()
{
     int i=0,j=0,r,w;
     char* sbuf;
     struct task_struct* PCB = (struct task_struct*)process_queue[run_queue[current]];
     sbuf=PCB->stderr;
     r=PCB->last_eread;
     w=PCB->last_ewrite;
     if(r<=0) return;
     for(i=r;i<=w;i++) {
                        sbuf[j++]=sbuf[i];
                       }
     PCB->last_eread=0;
     PCB->last_ewrite=w-r;
     //printf("[SYSTEM_MESSAGE]Flushed Buffer: %d %d",PCB->last_read,PCB->last_write);
     
}
uint64_t doRead(uint64_t source,uint64_t buffer,uint64_t size)
{
         uint32_t currentPID = getCurrentPID(),i=0;
         char* buf = (char*)buffer;
         if(source==0) {
                        if(currentPID!=foregroundProcess || size>1023) {
                                                           printf("[SYSTEM_MESSAGE]Illegel Read operation. Process will exit.\n");
                                                           exit_current_process();
                                                           return -1;
                                                          }
                        else {
                              struct task_struct* PCB = (struct task_struct*)process_queue[currentPID];
                              for(i=0;i<size;i++) {
                                                  if(PCB->last_read<PCB->last_write) {
                                                                                      PCB->last_read++;
                                                                                      buf[i]=PCB->stdin[PCB->last_read];
                                                                                      //printf("READ: %c\n",buf[i]);
                                                                                      //PCB->last_read++;
                                                                                      if(buf[i]=='\n') {
                                                                                                        buf[i]=0;
                                                                                                        i=strclean(buf);
                                                                                                        flushBuffer();
                                                                                                        return i;
                                                                                      }
                                                  }
                                                  else {
                                                        i--;
                                                        waiting_to_read[currentPID]=1;
                                                        asm("int $0x90;");
                                                       }
                              }
                              buf[size]=0;
                              i=strclean(buf);
                              flushBuffer();
                              return i;
                              
                              
                             }
                       }
         else if(source==2) { //stderr
                              struct task_struct* PCB = (struct task_struct*)process_queue[currentPID];
                              for(i=0;i<size;i++) {
                                                  if(PCB->last_eread<PCB->last_ewrite) {
                                                                                      PCB->last_eread++;
                                                                                      buf[i]=PCB->stderr[PCB->last_eread];
                                                                                      
                                                  }
                                                  else {
                                                        buf[i]=0;
                                                        flushErrBuffer();
                                                        return i;
                                                       }
                              }
                              buf[size]=0;
                              flushErrBuffer();
                              return i;
         }
         else {
               int d1=source;
               if(d1<3 || d1>=dir_desc_count) return -1;
               else {
                       struct task_struct* PCB = (struct task_struct*)process_queue[currentPID];
                       if(!PCB->tarfs_desc[d1]) return -1;
                       int p = PCB->tarfs_read_ptr[d1];
                       if(p>=dir_desc_size[d1]) return 0;
                       for(i=0;i<size;i++) {
                                            buf[i]=*((char*)(dir_desc_addr[d1]+p));
                                            if(buf[i]=='\n'||p>=dir_desc_size[d1]||buf[i]==-1) { //-1 = EOF
                                                                                    buf[i]=0;
                                                                                    p++;
                                                                                    PCB->tarfs_read_ptr[d1]=p;
                                                                                    return i;
                                                                                   }
                                            p++;
                                           }
                       buf[i]=0;
                       PCB->tarfs_read_ptr[d1]=p;
                       return i;
              }
              }
         return -1;
}
uint64_t doWrite(uint64_t source,uint64_t buffer,uint64_t size)
{
         uint32_t currentPID = getCurrentPID(),i=0;
         char* buf = (char*)buffer;
         if(source==1) {
                        // add code to check if backing is there
                        if(size>100) return -1;
                        for(i=0;i<size;i++) putchar(buf[i]);
                        return i;
                       }
         else if(source==2) {
              struct task_struct* PCB = (struct task_struct*)process_queue[currentPID];
              if(size>100) return -1;
              for(i=0;i<size;i++) {
                                   if(PCB->last_ewrite>500) return -1; // buffer overflow
                                   PCB->last_ewrite++;
                                   
                                   PCB->stderr[PCB->last_ewrite]=buf[i];
                                  }
         }
         return -1;
}
uint64_t doOpenDir(uint64_t dir)
{
         uint32_t currentPID = getCurrentPID();
         char* chardir = (char*)dir;
         int a=tarfs_opendir(chardir);
         if(a==-1) return -1;
         else {
               struct task_struct* PCB = (struct task_struct*)process_queue[currentPID];
               PCB->tarfs_desc[a]=1;
               PCB->tarfs_read_ptr[a]=0;
               return a;
              }
         return -1;
}

uint64_t doReadDir(uint64_t d)
{
         uint32_t currentPID = getCurrentPID();
         
         int d1=d;
         if(d1<3 || d1>=dir_desc_count) return -1;
         else {
               struct task_struct* PCB = (struct task_struct*)process_queue[currentPID];
               if(!PCB->tarfs_desc[d1]) return -1;
               int p = PCB->tarfs_read_ptr[d1];
               while(p<dir_desc_count) {
                                         p++;
                                         if(dir_desc_parent[p]==d1) {
                                                                        PCB->tarfs_read_ptr[d1]=p;
                                                                        return p;
                                                                       }
                                        }
               return 0;
              }
         return -1;
}
uint64_t doCloseDir(uint64_t d)
{
         uint32_t currentPID = getCurrentPID();
         
         int d1=d;
         if(d1<3 || d1>=dir_desc_count) return -1;
         else {
               struct task_struct* PCB = (struct task_struct*)process_queue[currentPID];
               PCB->tarfs_desc[d1]=0;
               PCB->tarfs_read_ptr[d1]=0;
               return d1;
              }
         return -1;
}
void doPrintDirEnt(uint64_t d)
{
     int d1=d;
     if(d1<3 || d1>=dir_desc_count) return;
     else {
               tarfs_printentry(d1);
     }
}
void getCurrentDirectory(uint64_t dir)
{
     char* sdir = (char*)dir;
     strcpy(sdir,current_directory);
}
void setDirectory(uint64_t dir)
{
     char* sdir = (char*)dir;
     char appended[100]="";
     //printf("[SYSTEM_MESSAGE]SDIR=%s\n",sdir);
     if(equal(sdir,"..")) {
                           if(current_directory[0]==0) return;
                           int p = getParent(current_directory);
                           strcpy(current_directory,dir_desc_name[p]);
                           return;
                          }
     strcpy(appended,current_directory);
     strappend(appended,sdir);
     int a = getIndex(appended);
     if(a && dir_desc_size[a]==0) strcpy(current_directory,appended);
     else printf("Directory %s does not exist\n",appended);
}
uint64_t doOpenFile(uint64_t file)
{
         uint32_t currentPID = getCurrentPID();
         char* charfile = (char*)file;
         int a=tarfs_openfile(charfile);
         if(a==-1) return -1;
         else {
               struct task_struct* PCB = (struct task_struct*)process_queue[currentPID];
               PCB->tarfs_desc[a]=1;
               PCB->tarfs_read_ptr[a]=0;
               return a;
              }
         return -1;
}
uint64_t doCloseFile(uint64_t d)
{
         uint32_t currentPID = getCurrentPID();
         
         int d1=d;
         if(d1<3 || d1>=dir_desc_count) return -1;
         else {
               struct task_struct* PCB = (struct task_struct*)process_queue[currentPID];
               PCB->tarfs_desc[d1]=0;
               PCB->tarfs_read_ptr[d1]=0;
               return d1;
              }
         return -1;
}
void listCurrentProcesses()
{
 int i;
 for(i=1;i<=process_count;i++) {
                                struct task_struct* PCB = (struct task_struct*)process_queue[run_queue[i]];
                                printf("%d %s\n",run_queue[i],PCB->procName);
                               }
}
