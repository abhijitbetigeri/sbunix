#include <defs.h>
#include "virtmemorymanager.h"
#include "memorymanager.h"
#include "scheduler.h"

#define VMM_BLOCK_SIZE 4096 // assuming block size of 4 KB

static	uint32_t _vmm_used_blocks=0; // number of used blocks
static	uint32_t _vmm_max_blocks=0; // maximum number of blocks
static	uint32_t* _vmm_memory_map; // We are mapping a 2GB virtual address space which is 524288 pages long. This requires
                                        // 524288 bits or 16384 32-bit entries
                                        
uint64_t malloc_sz;

extern int printf(const char *format,...);

inline void vmm_set(int block) // sets the bit corresponding to the block number
{ 
       //int i;
       _vmm_memory_map[block/32] |= (1<<(block%32));
       //uint64_t block_addr = block*VMM_BLOCK_SIZE;
       //for(i=0;i<4096;i++) *((uint64_t*)(block_addr+i))=0;
}

inline void vmm_unset(int block) // unsets the bit corresponding to the block number
{ 
       _vmm_memory_map[block/32] &= ~(1<<(block%32));
}

inline char vmm_isset(int block) // returns the bit (as true/false) corresponding to the block number
{ 
       return (_vmm_memory_map[block/32] & (1<<(block%32)))?1:0;
}

void vmm_init_region (uint64_t base, uint64_t length) // sets the blocks in a memory region as free
{
	int block_number = base/VMM_BLOCK_SIZE; // the block number corresponding to the memory location
	int blocks = length/VMM_BLOCK_SIZE;
	for (;blocks>0;blocks--) {
		vmm_unset(block_number);
		block_number++;
		_vmm_used_blocks--;
	}
	vmm_set(0);	//first block is always set. This insures allocs cant be 0 ??
}

void vmm_uninit_region (uint64_t base, uint64_t length) // sets the blocks in a memory region as used
{
	//printf("MM_UNINIT : %p %d\n",base,length);
    int block_number = base/VMM_BLOCK_SIZE; // the block number corresponding to the memory location
	int blocks = (length%VMM_BLOCK_SIZE)?(length/VMM_BLOCK_SIZE+1):(length/VMM_BLOCK_SIZE);
	//printf("Block Number = %d blocks = %d\n",block_number,blocks);
	for (;blocks>0;blocks--) {
        //printf("Block %d set",block_number);
		if(!vmm_isset(block_number)) 
        {
         vmm_set(block_number);
		 _vmm_used_blocks++;
        }
        block_number++;
	}
	vmm_set(0);	//first block is always set. This insures allocs cant be 0 ??
}

void vmm_init(uint64_t memSize,uint32_t* bitmap)
{
     int i;
     _vmm_memory_map = (uint32_t*)bitmap;
     _vmm_used_blocks=_vmm_max_blocks = memSize/VMM_BLOCK_SIZE;
     for(i=0;i<_vmm_max_blocks;i++) vmm_set(i);
}

int vmm_first_free() 
{
    uint32_t i,j,bit;
    for (i=0; i<_vmm_max_blocks/32; i++)
		if (_vmm_memory_map[i] != 0xffffffff)
			for (j=0; j<32; j++) {
				bit = 1 << j;
				if(!(_vmm_memory_map[i]&bit))
					return i*32+j;
			}
	return -1;
}

uint64_t * vmm_alloc_block() 
{
	int frame;
	uint64_t addr;
    if (_vmm_max_blocks == _vmm_used_blocks)
		return 0;	//out of memory 
	frame = vmm_first_free();
	if (frame == -1)
		return 0;	//out of memory
	vmm_set(frame);
	addr = frame * VMM_BLOCK_SIZE;
	_vmm_used_blocks++;
	return (uint64_t*)addr;
}

uint64_t * vmm_alloc_blocks(uint32_t size) // size is in bytes 
{
    int frame,i;
    int number_of_blocks = (size % VMM_BLOCK_SIZE)?(size/VMM_BLOCK_SIZE)+1 : size/VMM_BLOCK_SIZE;
    char found=1;
	uint64_t addr;
	//uint64_t* paddr;
    if (_vmm_max_blocks == _vmm_used_blocks)
		return 0;	//out of memory 
	frame = vmm_first_free();
	if (frame == -1)
		return 0;	//out of memory
	while(1) {
                for(i=0;i<number_of_blocks;i++) {
                                                   if(vmm_isset(frame+i)) {found=0; break;}
                }
                if(found) break;
                frame++;
                if((frame+number_of_blocks-1)>_vmm_max_blocks) break;
                found=1;
    }
    if(!found) return 0;
	for(i=0;i<number_of_blocks;i++) {
                                     vmm_set(frame+i);_vmm_used_blocks++;
                                     //addr = (frame+i) * VMM_BLOCK_SIZE;
                                     //paddr = (uint64_t*)addr;
                                     //for(j=0;j<4096;j++) paddr[j] = 0;
    }
	addr = frame * VMM_BLOCK_SIZE;
	return (uint64_t*)addr;
}

void vmm_free_blocks(uint64_t* p,uint32_t size) 
{
    int frame,i,n;
    n = (size % VMM_BLOCK_SIZE)?(size/VMM_BLOCK_SIZE)+1 : size/VMM_BLOCK_SIZE;
	uint64_t addr;
	addr = (uint64_t)p;
	frame = addr/VMM_BLOCK_SIZE;
	for(i=0;i<n;i++) {
     	vmm_unset(frame+i); 
    	_vmm_used_blocks--;
    }
}

volatile uint64_t* getPML4E(uint64_t* vaddr)
{
     volatile uint64_t offset = (0x1ff&((uint64_t)vaddr>>39));
     volatile uint64_t *pml4e = PML4T_ADDR + offset;
     return pml4e;
}

/* If corresponding entry not found in PML4E, it creates a new PDP and initializes PML4E */
volatile uint64_t* getPDP(uint64_t* vaddr)
{
     int i;
     volatile uint64_t pml4e_offset = (0x1ff&((uint64_t)vaddr>>39));
     volatile uint64_t *pml4e = getPML4E(vaddr);
     volatile uint64_t *pdp;
     if(!(*pml4e & PT_PRESENT_FLAG)) // allocate PDPT if not present
     {
        //printf("allocating page for PDPE");
        if(!(*pml4e = (uint64_t)(pdp = mm_alloc_block())))
                return 0; // out of memory
        *pml4e |= PT_PRESENT_FLAG | PT_WRITABLE_FLAG;
         pdp = (uint64_t*)((uint64_t)PDPT_ADDR+(pml4e_offset<<12));
        //Make all PDP entries 0
        for(i=0;i<512;i++) {
                            pdp[i]=0;
                            //printf("%d %p\n",i,pdp[i]);
        }
     }
     pdp = (uint64_t*)((uint64_t)PDPT_ADDR+(pml4e_offset<<12));
     
     return pdp;
}
volatile uint64_t* getPDPE(uint64_t* vaddr)
{
     volatile uint64_t pdpe_offset = (0x1ff&((uint64_t)vaddr>>30));
     volatile uint64_t *pdpe;
     pdpe = (uint64_t*)(getPDP(vaddr))+pdpe_offset;
     return pdpe;
}

volatile uint64_t* getPD(uint64_t* vaddr)
{
     int i;
     volatile uint64_t pml4e_offset = (0x1ff&((uint64_t)vaddr>>39));
     volatile uint64_t pdpe_offset = (0x1ff&((uint64_t)vaddr>>30));
     volatile uint64_t *pdpe = getPDPE(vaddr);
     volatile uint64_t *pd;
     if(!(*pdpe & PT_PRESENT_FLAG)) // allocate PDT if not present
     {
        //printf("allocating page for PDE");
        if(!(*pdpe = (uint64_t)(pd = mm_alloc_block())))
                return 0; // out of memory
        *pdpe |= PT_PRESENT_FLAG | PT_WRITABLE_FLAG;
        pd = (uint64_t*)((uint64_t)PGDIR_ADDR+(pml4e_offset<<21)+(pdpe_offset<<12));
        // Make all PD entries 0
        for(i=0;i<512;i++) {
                            pd[i]=0;
                            //printf("PD: %d %p\n",i,pd[i]);
        }
     }
     pd = (uint64_t*)((uint64_t)PGDIR_ADDR+(pml4e_offset<<21)+(pdpe_offset<<12));
     
     return pd;
}

volatile uint64_t* getPDE(uint64_t* vaddr)
{
     volatile uint64_t pde_offset = (0x1ff&((uint64_t)vaddr>>21));
     volatile uint64_t *pde;
     pde = (uint64_t*)(getPD(vaddr))+pde_offset;
     return pde;
}

volatile uint64_t* getPT(uint64_t* vaddr)
{
     int i;
     volatile uint64_t pml4e_offset = (0x1ff&((uint64_t)vaddr>>39));
     volatile uint64_t pdpe_offset = (0x1ff&((uint64_t)vaddr>>30));
     volatile uint64_t pde_offset = (0x1ff&((uint64_t)vaddr>>21));
     volatile uint64_t *pde = getPDE(vaddr);
     volatile uint64_t *pt;
     if(!(*pde & PT_PRESENT_FLAG)) // allocate PT if not present
     {
        //printf("allocating page for PTE");
        if(!(*pde = (uint64_t)(pt = mm_alloc_block())))
                return 0; // out of memory
        *pde |= PT_PRESENT_FLAG | PT_WRITABLE_FLAG;
        pt = (uint64_t*)((uint64_t)PGTBL_ADDR+(pml4e_offset<<30)+(pdpe_offset<<21)+(pde_offset<<12));
         // Make all entries 0
        for(i=0;i<512;i++) {
                            pt[i]=0;
                            //printf("PT: %d %p\n",i,pt[i]);
        }
     }
     pt = (uint64_t*)((uint64_t)PGTBL_ADDR+(pml4e_offset<<30)+(pdpe_offset<<21)+(pde_offset<<12));
     return pt;
}

volatile uint64_t* getPTE(uint64_t* vaddr)
{
     volatile uint64_t pte_offset = (0x1ff&((uint64_t)vaddr>>12));
     volatile uint64_t *pte;
     pte = (uint64_t*)(getPT(vaddr))+pte_offset;
     return pte;
}

uint32_t back_with_physical(uint64_t* adr) // **IMP** adr must be 4K aligned. backs a virtual page with a physical page. If a backing is already present, it zeroes out the page
{
     //TO DO: if ALIGN_DOWN(content of PML4 entry) is same as CR3 content, return 0
     uint32_t j;
     volatile uint64_t *pte,*page;
     pte = getPTE(adr);
     if(!(*pte & PT_PRESENT_FLAG))
     {
      page = mm_alloc_block();
      if(!page) return 0; // out of memory
      *pte = (uint64_t)page | PT_PRESENT_FLAG | PT_WRITABLE_FLAG | PT_TERMINAL_FLAG;
      
     }
     for(j=0;j<512;j++) adr[j]=0;
     return 1;
}
uint32_t back_with_physical_addr(uint64_t* adr) // **IMP** adr must be 4K aligned. backs a virtual page with a physical page. If a backing is already present, it zeroes out the page
{
     //TO DO: if ALIGN_DOWN(content of PML4 entry) is same as CR3 content, return 0
     uint32_t j;
     volatile uint64_t *pte,*page;
     pte = getPTE(adr);
     if(!(*pte & PT_PRESENT_FLAG))
     {
      page = mm_alloc_block();
      if(!page) return 0; // out of memory
      *pte = (uint64_t)page | PT_PRESENT_FLAG | PT_WRITABLE_FLAG | PT_TERMINAL_FLAG;
      return *pte;
     }
     for(j=0;j<512;j++) adr[j]=0;
     return 1;
}

uint64_t* pcb_malloc(uint32_t size)
{
     uint32_t i,r,number_of_pages = (size % VMM_BLOCK_SIZE)?(size/VMM_BLOCK_SIZE)+1 : size/VMM_BLOCK_SIZE;
     uint64_t *adr,*adr1,*adr2;
     adr2=vmm_alloc_blocks(size);
     if(!adr2) return 0; // out of memory
     //printf("DEBUG: PCB_MALLOC: vmm_alloc_blocks returns : %p\n",adr2);
     adr1=adr=(uint64_t*)(VIRT_PCB_MEM+(uint64_t)adr2);
     for(i=0;i<number_of_pages;i++) {
                                       r = back_with_physical(adr);
                                       if(!r) return 0; // out of memory
                                       adr+=512;
     }
     return adr1;
}

void pcb_free(uint64_t* p,uint32_t size) 
{
    int i,j,k,m,n;
    //int i,j,n;
    n = (size % VMM_BLOCK_SIZE)?(size/VMM_BLOCK_SIZE)+1 : size/VMM_BLOCK_SIZE;
	volatile uint64_t addr,addr1,*pte,*pt,*pde,*pd,*pdpe,*pdp,*pml4e;
	//uint64_t addr,addr1,*pte,*pt,pte_off;
	addr1 = addr = (uint64_t)p;
	for(i=0;i<n;i++) {
        pte = getPTE((uint64_t*)addr);
    	mm_free_block((uint64_t*)(*pte));
    	//pte_off = (0x1ff&((uint64_t)addr>>12));
    	pt = getPT((uint64_t*)addr);
    	//printf("DEBUG: pcb_free removing PT: pt[%d] = %p, \n",pte_off,pt[pte_off]);
    	*pte=0;
    	pt = getPT((uint64_t*)addr);
    	//printf("DEBUG: pcb_free removing PT: pt[%d] = %p, \n",pte_off,pt[pte_off]);
        for(j=0;j<512;j++) if(pt[j]) break;
        //printf("DEBUG: pcb_free removing PT: j = %d \n",j);
        if(j==512) {
                    pde = getPDE((uint64_t*)addr);
                    mm_free_block((uint64_t*)(*pde));
        	        *pde=0;
        	        pd = getPD((uint64_t*)addr);
                    for(k=0;k<512;k++) if(pd[k]) break;
                    //printf("DEBUG: pcb_free removing PD: k = %d\n",k);
                    //for(m=0;m<512;m++) {printf("%p ",pd[m]);for(j=0;j<1000000;j++);}
                    if(k==512) {
                                pdpe = getPDPE((uint64_t*)addr);
                                mm_free_block((uint64_t*)(*pdpe));
                    	        *pdpe=0;
                    	        pdp = getPDP((uint64_t*)addr);
                                for(m=0;m<512;m++) if(pdp[m]) break;
                                //printf("DEBUG: pcb_free removing PDP: m = %d\n",m);
                                if(m==512) {
                                            pml4e = getPML4E((uint64_t*)addr);
                                            mm_free_block((uint64_t*)(*pml4e));
          	                                *pml4e=0;
                                           }
                               }
                   }
        addr=(uint64_t)((uint64_t*)addr+512);
    }
    
	addr1 -=VIRT_PCB_MEM;
	vmm_free_blocks((uint64_t*)addr1,size);
}

void free_page(uint64_t* adr)
{
    int j,k,m;
    volatile uint64_t addr,*pte,*pt,*pde,*pd,*pdpe,*pdp,*pml4e;
    addr = (uint64_t)adr;
    pte = getPTE((uint64_t*)addr);
	if((*pte & PT_PRESENT_FLAG)) 
    {
     //mm_free_block((uint64_t*)(*pte));
        int pgno = (int)(ALIGN_DOWN(*pte)/4096);
        int refcnt = get_ref_cnt(pgno);
        //printf("phys addr = %p. Refcnt = %d\n",ALIGN_DOWN(*pte),refcnt);
        if(refcnt==0)
                     mm_free_block((uint64_t*)(*pte));
        else
                     mm_dec_ref_cnt(pgno);
    }
	//pte_off = (0x1ff&((uint64_t)addr>>12));
	//pt = getPT((uint64_t*)addr);
	//printf("DEBUG: free_page removing PT: pt[%d] = %p, \n",pte_off,pt[pte_off]);
	*pte=0;
	pt = getPT((uint64_t*)addr);
	//printf("DEBUG: pcb_free removing PT: pt[%d] = %p, \n",pte_off,pt[pte_off]);
    for(j=0;j<512;j++) if(pt[j]) break;
    //printf("DEBUG: free_page removing PT: j = %d \n",j);
    if(j==512) {
                pde = getPDE((uint64_t*)addr);
                mm_free_block((uint64_t*)(*pde));
    	        *pde=0;
    	        pd = getPD((uint64_t*)addr);
                for(k=0;k<512;k++) if(pd[k]) break;
                //printf("DEBUG: free_page removing PD: k = %d\n",k);
                //for(m=0;m<512;m++) {printf("%p ",pd[m]);for(j=0;j<1000000;j++);}
                if(k==512) {
                            pdpe = getPDPE((uint64_t*)addr);
                            mm_free_block((uint64_t*)(*pdpe));
                	        *pdpe=0;
                	        pdp = getPDP((uint64_t*)addr);
                            for(m=0;m<512;m++) if(pdp[m]) break;
                            //printf("DEBUG: free_page removing PDP: m = %d\n",m);
                            if(m==512) {
                                        pml4e = getPML4E((uint64_t*)addr);
                                        mm_free_block((uint64_t*)(*pml4e));
                                        
      	                                *pml4e=0;
                                       }
                           }
               }
}
void alloc_mm_struct(struct task_struct* process_task_struct)
{
     char* zddr = (char*)(process_task_struct+1);
     process_task_struct->mm = (struct mm_struct*)zddr;
}

struct vm_area_struct * alloc_vma(struct mm_struct* mm)
{
     
     struct vm_area_struct * vma_last;
     if(mm->mmap==NULL) {
                         char* adr = (char*)(mm+1);
                         vma_last = (struct vm_area_struct *)adr;
                         
                         mm->mmap = vma_last;
                         mm->map_count++;
                         return vma_last;
                        }
     vma_last = mm->mmap;
     while(vma_last->vm_next!=NULL) vma_last = vma_last->vm_next;
     char * adr = (char*)vma_last+sizeof(struct vm_area_struct)+sizeof(struct vm_area_struct);
     if((ALIGN_DOWN((uint64_t)adr)) == (ALIGN_DOWN((uint64_t)vma_last))) // check if the new vma will fit in the same page
     {
      adr-=sizeof(struct vm_area_struct);
      vma_last->vm_next=(struct vm_area_struct*)adr;
      mm->map_count++;
      return (struct vm_area_struct*)adr;
     }
     else {
           char* adr = (char*)pcb_malloc(1); // allocate a new page
           if(!adr) return 0; // out of memory;
           vma_last->vm_next=(struct vm_area_struct*)adr;
           mm->map_count++;
           return (struct vm_area_struct*)adr;
          }
}

void get_malloc_size(uint64_t arg1)
{
    //printf("Received: MALLOC_SIZE: %p\n",arg1);
    malloc_sz=arg1;
}

uint64_t smalloc()
{
    uint64_t a;
    a=sbrk1();
    if(a) return a;
    return 0;
}

uint64_t sbrk1()
{
    struct task_struct* current_task_struct;
    current_task_struct=getCurrentTaskStruct();
    struct vm_area_struct* vma = current_task_struct->mm->mmap;
    while(vma!=NULL) {
                      //printf("VMA: FLST = %p, 
                      if(vma->vm_filestart==NULL && (vma->vm_flags & VM_READ) && (vma->vm_flags & VM_WRITE) && !(vma->vm_flags & VM_GROWSDOWN) && vma->vm_start!=0x0000fa8000000000) break;
                      vma=vma->vm_next;                
                   }
    //printf("VMA: %p\n",vma);
    //printf("HEAP1 Start: %p End: %p size requested: %d\n",vma->vm_start,vma->vm_end,malloc_sz);
    if((vma->vm_end+malloc_sz)<0x0000fa0000000000) {
                                                    uint64_t prev = vma->vm_end;
                                                    vma->vm_end = vma->vm_end + malloc_sz;
                                                    return prev;
                                                   }
    return 0;
}

uint64_t sbrk2() // Deprecated
{
    struct task_struct* current_task_struct;
    current_task_struct=getCurrentTaskStruct();
    struct vm_area_struct* vma = current_task_struct->mm->mmap;
    struct vm_area_struct* vma_stack = current_task_struct->mm->mmap;
    while(vma!=NULL) {
                      if(vma->vm_filestart==NULL && (vma->vm_flags & VM_READ) && (vma->vm_flags & VM_WRITE) && !(vma->vm_flags & VM_GROWSDOWN) && vma->vm_start==0x0000fa8000000000) break;
                      vma=vma->vm_next;                
                   }
    while(vma_stack!=NULL) {
                      if(vma_stack->vm_filestart==NULL && (vma_stack->vm_flags & VM_READ) && (vma_stack->vm_flags & VM_WRITE) && (vma_stack->vm_flags & VM_GROWSDOWN)) break;
                      vma_stack=vma_stack->vm_next;                
                   }
    printf("VMA: %p\n",vma);
    printf("HEAP2 Start: %p End: %p size requested: %d\n",vma->vm_start,vma->vm_end,malloc_sz);
    printf("VMA: %p\n",vma_stack->vm_start);
    printf("HEAP2 Start: %p End: %p size requested: %d\n",vma_stack->vm_start,vma_stack->vm_end,malloc_sz);
    if((vma->vm_end+malloc_sz)<(vma_stack->vm_start)) {
                                                    uint64_t prev = vma->vm_end;
                                                    vma->vm_end = vma->vm_end + malloc_sz;
                                                    return prev;
                                                   }
    return 0;
}

void vmm_print_attributes() // just a debug function
{
     printf("\n\nUsed or Reserved Blocks: %d\nFree Blocks: %d\nTotal Blocks: %d\n\n",_vmm_used_blocks,_vmm_max_blocks-_vmm_used_blocks,_vmm_max_blocks);
}

