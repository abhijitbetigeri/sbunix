/* Reference: http://www.brokenthorn.com/Resources/OSDev17.html */
#include <defs.h>
#include "memorymanager.h"

#define MM_BLOCK_SIZE 4096 // assuming block size of 4 KB
#define PAGE_TABLE_SIZE 512*64 // 512 64-bit entries
#define PAGE_TABLE_ALIGNLENT 0x1000

#define ALIGN_DOWN(x)   ((x) & ~(PAGE_TABLE_ALIGNLENT-1))

static	uint32_t _mm_used_blocks=0; // number of used blocks
static	uint32_t _mm_max_blocks=0; // maximum number of blocks
static	uint32_t* _mm_memory_map=0; // memory map bit array. Each bit represents a memory block. Since there are 32766 blocks,
                                       // we need 32766/32 = 1024 32-bit spaces to store the entire memory map
static  unsigned char* _mm_page_ref=0;
extern int printf(const char *format,...);

uint64_t prev_pte; // For DEBUGGING

uint64_t* bump_ptr = (uint64_t*)0x0000000000000000+512; // Memory pointer for bump allocator. Starting from 2nd page

inline void mm_set(int block) // sets the bit corresponding to the block number
{ 
       //int i;
       _mm_memory_map[block/32] |= (1<<(block%32));
       //uint64_t block_addr = block*MM_BLOCK_SIZE;
       //for(i=0;i<4096;i++) *((uint64_t*)(block_addr+i))=0;
}

inline void mm_inc_ref_cnt(int block) // sets the bit corresponding to the block number
{ 
       //int i;
       _mm_page_ref[block]++;
       //uint64_t block_addr = block*MM_BLOCK_SIZE;
       //for(i=0;i<4096;i++) *((uint64_t*)(block_addr+i))=0;
}

inline void mm_unset(int block) // unsets the bit corresponding to the block number
{ 
       //if(block==0) printf("WARNING: Unsetting 0th block!\n");
       _mm_memory_map[block/32] &= ~(1<<(block%32));
}

inline void mm_dec_ref_cnt(int block) // unsets the bit corresponding to the block number
{ 
       //if(block==0) printf("WARNING: Unsetting 0th block!\n");
       _mm_page_ref[block]--;
}

inline char mm_isset(int block) // returns the bit (as true/false) corresponding to the block number
{ 
       return (_mm_memory_map[block/32] & (1<<(block%32)))?1:0;
}

inline unsigned char get_ref_cnt(int block) // returns the bit (as true/false) corresponding to the block number
{ 
       return _mm_page_ref[block];
}

void mm_init_region (uint64_t base, uint64_t length) // sets the blocks in a memory region as free
{
	int block_number = base/MM_BLOCK_SIZE; // the block number corresponding to the memory location
	int blocks = length/MM_BLOCK_SIZE;
	for (;blocks>0;blocks--) {
		mm_unset(block_number);
		block_number++;
		_mm_used_blocks--;
	}
	mm_set(0);	//first block is always set. This insures allocs cant be 0 ??
}

void mm_uninit_region (uint64_t base, uint64_t length) // sets the blocks in a memory region as used
{
	//printf("MM_UNINIT : %p %d\n",base,length);
    int block_number = base/MM_BLOCK_SIZE; // the block number corresponding to the memory location
	int blocks = (length%MM_BLOCK_SIZE)?(length/MM_BLOCK_SIZE+1):(length/MM_BLOCK_SIZE);
	//printf("Block Number = %d blocks = %d\n",block_number,blocks);
	for (;blocks>0;blocks--) {
        //printf("Block %d set",block_number);
		if(!mm_isset(block_number)) 
        {
         mm_set(block_number);
		 _mm_used_blocks++;
        }
        block_number++;
	}
	mm_set(0);	//first block is always set. This insures allocs cant be 0 ??
}

void mm_init(uint64_t memSize,uint32_t* bitmap)
{
     int i;
     _mm_memory_map = (uint32_t*)bitmap;
     _mm_used_blocks=_mm_max_blocks = memSize/MM_BLOCK_SIZE;
     for(i=0;i<_mm_max_blocks;i++) mm_set(i);
}

void mm_init_pageref(uint64_t memSize,unsigned char* bitmap)
{
     int i;
     _mm_page_ref = bitmap;
     for(i=0;i<_mm_max_blocks;i++) _mm_page_ref[i]=0;
}

int mm_first_free() 
{
    uint32_t i,j,bit;
    for (i=0; i<_mm_max_blocks/32; i++)
		if (_mm_memory_map[i] != 0xffffffff)
			for (j=0; j<32; j++) {
				bit = 1 << j;
				if(!(_mm_memory_map[i]&bit))
					return i*32+j;
			}
	return -1;
}

uint64_t * mm_alloc_block() 
{
	int frame;
	uint64_t addr;
	//printf("DEBUG: MM_ALLOC_BLOCK()\n");
	//mm_print_attributes();
    if (_mm_max_blocks == _mm_used_blocks)
		return 0;	//out of memory 
	frame = mm_first_free();
	//printf("FRAME = %d\n",frame);
	if (frame == -1)
		return 0;	//out of memory
	mm_set(frame);
	addr = frame * MM_BLOCK_SIZE;
	_mm_used_blocks++;
	return (uint64_t*)addr;
}

uint64_t * mm_alloc_blocks(uint32_t size) // size is in bytes 
{
    int frame,i,j;
    int number_of_blocks = (size % MM_BLOCK_SIZE)?(size/MM_BLOCK_SIZE)+1 : size/MM_BLOCK_SIZE;
    char found=1;
	uint64_t addr;
	uint64_t* paddr;
    if (_mm_max_blocks == _mm_used_blocks)
		return 0;	//out of memory 
	frame = mm_first_free();
	if (frame == -1)
		return 0;	//out of memory
	while(1) {
                for(i=0;i<number_of_blocks;i++) {
                                                   if(mm_isset(frame+i)) {found=0; break;}
                }
                if(found) break;
                frame++;
                if((frame+number_of_blocks-1)>_mm_max_blocks) break;
                found=1;
    }
    if(!found) return 0;
	for(i=0;i<number_of_blocks;i++) {
                                     mm_set(frame+i);_mm_used_blocks++;
                                     addr = (frame+i) * MM_BLOCK_SIZE;
                                     paddr = (uint64_t*)addr;
                                     for(j=0;j<4096;j++) paddr[j] = 0;
    }
	addr = frame * MM_BLOCK_SIZE;
	return (uint64_t*)addr;
}

void mm_free_block(uint64_t* p) 
{
    int frame;
	uint64_t addr;
	addr = (uint64_t)p;
	frame = addr/MM_BLOCK_SIZE;
 	mm_unset(frame); 
	_mm_used_blocks--;
}

void mm_print_attributes() // just a debug function
{
     printf("\n\nUsed or Reserved Blocks: %d\nFree Blocks: %d\nTotal Blocks: %d\n\n",_mm_used_blocks,_mm_max_blocks-_mm_used_blocks,_mm_max_blocks);
}

/* Functions for memory mapping */

int mmap(uint64_t* phy_addr, uint64_t* virt_addr, struct page_table_mem *tab)
{
     uint64_t *pml4e;
     uint64_t *pdpe;
     uint64_t *pde; 
     uint64_t *pte;
     //printf("Physical Address: %p\n",phy_addr);
     //printf("Virtual Address: %p\n",virt_addr);
     /*** find or allocate the pml4e ***/
     if(!(pml4e = tab->pml4e) && !(pml4e = tab->pml4e = mm_alloc_block()))
                return -1;
     //printf("pml4e address = %p\n",pml4e);
     // Self Referencing: Make the 500th entry point to itself
     *(pml4e+500)=(uint64_t)pml4e | PT_PRESENT_FLAG | PT_WRITABLE_FLAG;
     
     pml4e += (0x1ff&((uint64_t)virt_addr>>39));
     //printf("shifting pml4e address by %p bytes\n",(0x1ff&((uint64_t)virt_addr>>39))*8);
     //printf("shifted pml4e address = %p\n",pml4e);
     /*** find or allocate the pdpe ***/
     if(!(*pml4e & PT_PRESENT_FLAG)) {
                if(!(*pml4e = (uint64_t)(pdpe = mm_alloc_block())))
                        return -1; // out of memory
     }
     else 
     {
      pdpe = (uint64_t*)ALIGN_DOWN(*pml4e);
     }
     //printf("pdpe address = %p\n",pdpe);
     //printf("pml4e content = %p\n",*pml4e);
     // point pml4e to pdpe
     *pml4e |= PT_PRESENT_FLAG | PT_WRITABLE_FLAG;
     
     pdpe += (0x1ff&((uint64_t)virt_addr>>30));
     //printf("shifting pdpe address by %d bytes\n",(0x1ff&((uint64_t)virt_addr>>30))*8);
     //printf("shifted pdpe address = %p\n",pdpe);
     /*** find or allocate the pdpe ***/
     if(!(*pdpe & PT_PRESENT_FLAG)) {
                if(!(*pdpe = (uint64_t)(pde = mm_alloc_block())))
                        return -1; // out of memory
     }
     else 
     {
      pde = (uint64_t*)ALIGN_DOWN(*pdpe);
     }
     //printf("pde address = %p\n",pde);
     //printf("pdpe content = %p\n",*pdpe);
     // point pdpe to pde
     *pdpe |= PT_PRESENT_FLAG | PT_WRITABLE_FLAG;
     pde += (0x1ff&((uint64_t)virt_addr>>21));
     //printf("shifting pde address by %d bytes\n",(0x1ff&((uint64_t)virt_addr>>21))*8);
     //printf("shifted pde address = %p\n",pde);
     /*** find or allocate the pte ***/
     if(!(*pde & PT_PRESENT_FLAG)) {
                if(!(*pde = (uint64_t)(pte = mm_alloc_block())))
                        return -1; // out of memory
     }
     else 
     {
      pte = (uint64_t*)ALIGN_DOWN(*pde);
     }
     //printf("pte address = %p\n",pte);
     //printf("pde content = %p\n",*pde);
     // point pdpe to pde
     *pde |= PT_PRESENT_FLAG | PT_WRITABLE_FLAG;
     pte += (0x1ff&((uint64_t)virt_addr>>12));
     //printf("shifting pte address by %d bytes\n",(0x1ff&((uint64_t)virt_addr>>12))*8);
     //printf("shifted pte address = %p\n",pte);
     // point pte to physical address.
     *pte = (((uint64_t)phy_addr/MM_BLOCK_SIZE)*MM_BLOCK_SIZE) | PT_PRESENT_FLAG | PT_WRITABLE_FLAG | PT_TERMINAL_FLAG;
     //printf("pte content = %p\n",*pte);
     /*if(*pte!=prev_pte) {
                        printf("PHYSICAL ADDRESS: %p\n",phy_addr);
                        printf("VIRTUAL ADDRESS: %p\n",virt_addr);
                        printf("pml4(addr = %p) pml4e (addr = %p,content = %p) -> pdpe (addr = %p, content = %p) -> pde (addr = %p, content = %p) -> pte(addr = %p, content = %p)\n",tab->pml4e,pml4e,*pml4e,pdpe,*pdpe,pde,*pde,pte,*pte);
     }*/
     prev_pte = *pte;
     return 0;
}

/*uint64_t* bump_malloc(uint32_t size)
{
     uint32_t i,number_of_pages = (size % MM_BLOCK_SIZE)?(size/MM_BLOCK_SIZE)+1 : size/MM_BLOCK_SIZE;
     uint64_t *pte,*page,*bump_ptr1;
     bump_ptr1=bump_ptr;
     for(i=0;i<number_of_pages;i++) {
                                       pte = getPTE(bump_ptr);
                                       if(!(*pte & PT_PRESENT_FLAG))
                                       {
                                        page = mm_alloc_block();
                                        if(!page) return 0;
                                        *pte = (uint64_t)page | PT_PRESENT_FLAG | PT_WRITABLE_FLAG | PT_TERMINAL_FLAG;
                                       }
                                       bump_ptr+=512;
     }
     return bump_ptr1;
}*/
