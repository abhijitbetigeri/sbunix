#include <defs.h>
#include <syscall.h>
#include "inline.c"
#include "keyboard.h"
#include "scheduler.h"
#include "virtmemorymanager.h"
#include "memorymanager.h"
#define FALSE 0
#define TRUE 1

#define P 1
#define RW 1<<1
#define US 1<<2

extern int printf(const char *format,...);
extern void putchar(char);

extern void PIC_sendEOI(unsigned char);

void format(char * str, int a)
{
     str[0]='0'+a/10;str[1]='0'+a%10;str[2]=0;
}
void isr_handler()
{
  printf("Divide by 0 occurred"); 
}
void gpf_handler()
{
  printf("GPF occurred"); 
  asm volatile("hlt");
}
void pf_handler(uint64_t * tos)
{
  uint32_t err_code = tos[0];
  uint64_t fault_addr;
  asm volatile(                                                   
            	"movq %%cr2, %0;"                                               
            	:"=g"(fault_addr)          
                );
  if(!(err_code & US)) {
                if((ALIGN_DOWN(fault_addr))==0) {  
                                                 printf("PAGE FAULT IN KERNEL CODE: %p. Kernel Must Reeboot!\n",err_code);
                                                 asm("hlt");
                }
                else {
                      printf("PAGE FAULT IN KERNEL CODE: Segmentation Fault. No VMAs found for address %p.\n",fault_addr);
                      exit_current_process();
                     }
  }
  /*else if(err_code & P) {
       printf("Segmentation Fault. Illegal address: %p. Process will be killed.\n",fault_addr);
       exit_current_process();
  }*/
  else {
          struct task_struct* current_task_struct; 
          
          //printf("Page Fault occurred while accessing address: %p\n",fault_addr);
          //asm("hlt");
          current_task_struct=getCurrentTaskStruct();
          volatile uint64_t* fpml4e = getPML4E((uint64_t*)(fault_addr));
          if(*fpml4e & PT_COW) {
                     //printf("Page Fault occurred as a result of COW-fork: %p %p\n",err_code,fault_addr);
                     volatile uint64_t* pml4e = getPML4E((uint64_t*)(fault_addr));
                     volatile uint64_t* pdpe = getPDPE((uint64_t*)(fault_addr));
                     volatile uint64_t* pde = getPDE((uint64_t*)(fault_addr));
                     volatile uint64_t* pte = getPTE((uint64_t*)(fault_addr));
                     uint64_t physpage = ALIGN_DOWN(*pte);
                     int page_number = (int)(physpage/4096);
                     int refcnt = get_ref_cnt(page_number);
                     if(refcnt==0) {
                                    //printf("Page Ref count for %p is 0\n",physpage);
                                    *pte ^= PT_COW; // unset COW bit
                                    *pte |= PT_WRITABLE_FLAG; // set writable bit
                                    *pde ^= PT_COW; // unset COW bit
                                    *pde |= PT_WRITABLE_FLAG;
                                    *pdpe ^= PT_COW; // unset COW bit
                                    *pdpe |= PT_WRITABLE_FLAG;
                                    *pml4e ^= PT_COW; // unset COW bit
                                    *pml4e |= PT_WRITABLE_FLAG;
                                   }
                     else {
                           //printf("PID: %d Page Ref count for %p is %d. Must allocate a new page\n",getCurrentPID(),physpage,refcnt);
                           *pte ^= PT_COW; // unset COW bit
                           *pte |= PT_WRITABLE_FLAG; // set writable bit
                           *pde ^= PT_COW; // unset COW bit
                           *pde |= PT_WRITABLE_FLAG;
                           *pdpe ^= PT_COW; // unset COW bit
                           *pdpe |= PT_WRITABLE_FLAG;
                           *pml4e ^= PT_COW; // unset COW bit
                           *pml4e |= PT_WRITABLE_FLAG;
                           uint64_t buffer[512];
                           uint64_t* t = (uint64_t*)ALIGN_DOWN(fault_addr);
                           int i;
                           for(i=0;i<512;i++) 
                           {
                            buffer[i]=t[i];
                           }
                           mm_dec_ref_cnt(page_number);
                           refcnt = get_ref_cnt(page_number);
                           //printf("Page Ref count for %p is %d\n",physpage,refcnt);
                           uint64_t* newPhysPage = mm_alloc_block();
                           *pte = (uint64_t)newPhysPage | PT_PRESENT_FLAG | PT_WRITABLE_FLAG | PT_TERMINAL_FLAG | PT_USER_FLAG;
                           for(i=0;i<512;i++) t[i]=buffer[i];
                           //asm("hlt");
                          }
                     return;
          }
          struct vm_area_struct* vma = current_task_struct->mm->mmap;
          while(vma!=NULL) {
                            if(fault_addr>=vma->vm_start && fault_addr<vma->vm_end) break;
                            vma=vma->vm_next;                
                           }
          if(vma==NULL) {
                         printf("Segmentation Fault. No VMAs found for address %p.\n",fault_addr);
                         exit_current_process(); // Kill the process here
          }
          else if(vma->vm_filestart!=NULL) { // File Backed region
               int k=0,size=vma->vm_memsz;
               uint64_t vaddr = vma->vm_start;
               char* zddr1 = (char*)(vma->vm_filestart);
               uint64_t offset = vma->vm_pgoff;
               //printf("Found FILE VMA: vaddr=%p size=%p zddr=%p offset=%p\n",vaddr,size,zddr1,offset);
               do {
                    //printf("k=%d size=%d ",k,size);
                    //mm_print_attributes();
                    volatile uint64_t* pml4e = getPML4E((uint64_t*)(vaddr+k));
                    //printf("PML4E = %p\n",*pml4e);
                    //uint64_t* pdp = getPDP((uint64_t*)(vaddr+k));
                    //printf("PML4E = %p PDP = %p\n",*pml4e,*pdp);
                    volatile uint64_t* pdpe = getPDPE((uint64_t*)(vaddr+k));
                    //printf("PML4E = %p PDPE = %p\n",*pml4e,*pdpe);
                    volatile uint64_t* pde = getPDE((uint64_t*)(vaddr+k));
                    //printf("PML4E = %p PDPE = %p PDE = %p\n",*pml4e,*pdpe,*pde);
                    volatile uint64_t* pte = getPTE((uint64_t*)(vaddr+k));
                    //printf("PML4E = %p PDPE = %p PDE = %p PTE = %p\n",*pml4e,*pdpe,*pde,*pte);
                    
                    if(!(*pte & PT_PRESENT_FLAG)) {
                              int a=back_with_physical((uint64_t*)(ALIGN_DOWN(vaddr+k)));
                              if(!a) {
                                printf("Out of memory. Could not back up address %p.\n",fault_addr);
                                exit_current_process(); // Kill the process here
                               }
                              (*pte) = (*pte) | PT_USER_FLAG;
                              (*pde) = (*pde) | PT_USER_FLAG;
                              (*pdpe) = (*pdpe) | PT_USER_FLAG;
                              (*pml4e) = (*pml4e) | PT_USER_FLAG;
                    }
                    //volatile uint64_t* to = (uint64_t*)(vaddr+k);
                    //volatile uint64_t* from = (zddr1+offset+k);
                    //*to = *from;
                    //*((uint64_t*)(vaddr+k))=*(zddr1+offset+k);
                    *((char*)(vaddr+k))=*(zddr1+offset+k);
                    k++;
               }while(k<size);
               //printf("ADDR: %p \n",vaddr);
          }
          else {               // anon region
                
                volatile uint64_t* pte = getPTE((uint64_t*)fault_addr);
                volatile uint64_t* pde = getPDE((uint64_t*)fault_addr);
                volatile uint64_t* pdpe = getPDPE((uint64_t*)fault_addr);
                volatile uint64_t* pml4e = getPML4E((uint64_t*)fault_addr);
                
                //printf("DEBUG: Process Stack PTE=%p PDE=%p PDPE=%p PML4E=%p\n",*pte,*pde,*pdpe,*pml4e);
                (*pte) = (*pte) | PT_USER_FLAG;
                
                (*pde) = (*pde) | PT_USER_FLAG;
                
                (*pdpe) = (*pdpe) | PT_USER_FLAG;
                
                (*pml4e) = (*pml4e) | PT_USER_FLAG;
                int t=back_with_physical((uint64_t*)(ALIGN_DOWN(fault_addr)));
                if(!t) {
                        printf("Out of memory. Could not back up address %p.\n",fault_addr);
                        exit_current_process(); // Kill the process here
                       }
                //printf("Successfully backed address %p with physical page.\n",fault_addr);
               }
  }
}
void isr_handler_32()
{
  static int counter = 0;
  static long second = 1;
  volatile register char *pos = (char*)0xFFFFFFFF80300000;
  pos = pos+24*160-16;
  int H,M,S,i;
  counter++;
  //printf("counter = %d\n",counter);
  if(counter==18) {
      for(i=0;i<MAX_PROCESS_COUNT;i++) if(sleep_time[i]) {/*printf("sleep_time for process %d updated to %d",i,sleep_time[i]-1);*/sleep_time[i]--;}
      //for(i=0;i<=process_count;i++) printf("%d ",run_queue[i]);printf("\n");
      H = second/3600;
      M = (second%3600)/60;
      S = second%60;
      *pos = '0'+H/10;
      *(pos+2) = '0'+H%10;
      *(pos+4) = ':';
      *(pos+6) = '0'+M/10;
      *(pos+8) = '0'+M%10;
      *(pos+10) = ':';
      *(pos+12) = '0'+S/10;
      *(pos+14) = '0'+S%10;
      second++;
      counter=0;    
      //schedule();             
  }
  PIC_sendEOI(0);
}

void isr_handler_33()
{
  unsigned int scancode;
  char c;
  static unsigned int prev_scancode = 0;
  static char keypress_enabled = TRUE;
  //volatile register char *pos = (char*)0xFFFFFFFF80300000;
  //pos = pos+24*160-20;
  scancode = inb(0x60);
  if(scancode == 42)
      keypress_enabled=FALSE;
  else if (scancode & 0x80)
  {
   if((scancode^0x80)==42) {
                            //*pos = key_map_shift[prev_scancode^0x80];
                            c=key_map_shift[prev_scancode^0x80];
                            struct task_struct* PCB = (struct task_struct*)process_queue[foregroundProcess];
                            PCB->stdin[++PCB->last_write]=c;
                            putchar(PCB->stdin[PCB->last_write]);
                            keypress_enabled = TRUE;
                            
   }
   else 
        if(keypress_enabled) {
                              //*pos = key_map[scancode^0x80];
                              c = key_map[scancode^0x80];
                              struct task_struct* PCB = (struct task_struct*)process_queue[foregroundProcess];
                              PCB->stdin[++PCB->last_write]=c;
                              putchar(PCB->stdin[PCB->last_write]);
        }
  }
  //printf("INTERRUPT 33 occurred %x %d\n",scancode,scancode);
  /*if(scancode<128 && scancode!=42 && scancode!=29 && scancode!=56) {
                    printf("%c\n",key_map[scancode]);
                   }*/
  prev_scancode = scancode;
  PIC_sendEOI(1);
}
void syscall_handler()
{
  uint64_t n,arg1,ret=-1,arg2,arg3;
  __asm volatile("movq %%rax, %0;"
                 "movq %%rsi, %1;"
                 "movq %%rdi, %2;"
                 "movq %%rdx, %3;"
                 : "=g"(n),"=g"(arg1),"=g"(arg2),"=g"(arg3)
                 :
                 :"rax","rsi","rdi","rdx"
  );
  //asm volatile("movq %%rsi, %0;" : "=g"(arg1));
  //asm volatile("movq %%rdi, %0;" : "=g"(arg2));
  switch(n) {
             case SYS_PUTCHAR:
                              putchar((char)arg1);
                              asm volatile("movq %0, %%rax":: "b"(ret));
                              break;
             case SYS_EXIT:
                              get_exit_status(arg1);
                              exit_current_process();
                              asm volatile("movq %0, %%rax":: "b"(ret));
                              break;
             case SYS_GETPID:
                              ret = getCurrentPID();
                              //printf("Putting following PID in rax: %d\n",ret);
                              asm volatile("movq %0, %%rax":: "b"(ret));
                              break;
             case SYS_MALLOC:
                              get_malloc_size(arg1);
                              ret = smalloc();
                              //printf("Putting following PID in rax: %d\n",ret);
                              asm volatile("movq %0, %%rax":: "b"(ret));
                              break;
             case SYS_FORK:
                              ret = forkCurrentProcess();
                              //printf("Putting following PID in rax: %d\n",ret);
                              asm volatile("movq %0, %%rax":: "b"(ret));
                              break;
			case SYS_VIRT_PHY:
							  ret = back_with_physical_addr(arg1);
                              //printf("Putting following PID in rax: %d\n",ret);
                              asm volatile("movq %0, %%rax":: "b"(ret));
                              break;
							
             case SYS_EXECVPE:
                              ret = execvpeCurrentProcess(arg1,arg2,arg3);
                              //printf("Putting following PID in rax: %d\n",ret);
                              asm volatile("movq %0, %%rax":: "b"(ret));
                              break;
             case SYS_SLEEP:
                              ret = sleepCurrentProcess(arg1);
                              asm volatile("movq %0, %%rax":: "b"(ret));
                              break;
             case SYS_WAITPID:
                              getWaitPid(arg1,arg2);
                              //printf("Returned from getWaitPID %p %p\n",waitpid_ret_pid,waitpit_ret_exit_st);
                              *((uint32_t*)arg2) = waitpit_ret_exit_st;
                              asm volatile("movq %0, %%rax":: "b"((uint64_t)waitpid_ret_pid));
                              break;
             case SYS_READ:
                              ret = doRead(arg1,arg2,arg3);
                              asm volatile("movq %0, %%rax":: "b"(ret));
                              break;
             case SYS_WRITE:
                              ret = doWrite(arg1,arg2,arg3);
                              asm volatile("movq %0, %%rax":: "b"(ret));
                              break;
             case SYS_OPENDIR:
                              ret = doOpenDir(arg1);
                              asm volatile("movq %0, %%rax":: "b"(ret));
                              break;
             case SYS_READDIR:
                              ret = doReadDir(arg1);
                              asm volatile("movq %0, %%rax":: "b"(ret));
                              break;
             case SYS_CLOSEDIR:
                              ret = doCloseDir(arg1);
                              asm volatile("movq %0, %%rax":: "b"(ret));
                              break;
             case SYS_PRINTDIRENT:
                              doPrintDirEnt(arg1);
                              asm volatile("movq %0, %%rax":: "b"(ret));
                              break;
             case SYS_GETCURRDIR:
                              getCurrentDirectory(arg1);
                              asm volatile("movq %0, %%rax":: "b"(ret));
                              break;
             case SYS_SETCURRDIR:
                              setDirectory(arg1);
                              asm volatile("movq %0, %%rax":: "b"(ret));
                              break;
             case SYS_OPENFILE:
                              ret=doOpenFile(arg1);
                              asm volatile("movq %0, %%rax":: "b"(ret));
                              break;
             case SYS_CLOSEFILE:
                              ret=doCloseFile(arg1);
                              asm volatile("movq %0, %%rax":: "b"(ret));
                              break;
             case SYS_LISTPROCESSES:
                              listCurrentProcesses();
                              asm volatile("movq %0, %%rax":: "b"(ret));
                              break;
             default:
                              asm volatile("movq %0, %%rax":: "b"(ret));
                              break;
            }
}
void isr_handler_144()
{
  char c;
  unsigned long c1;
  asm volatile("movq %%rax, %0;" : "=g"(c1));
  c = c1;
  putchar(c);
}

