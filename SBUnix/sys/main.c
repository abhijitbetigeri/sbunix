#include <defs.h>
#include <sys/gdt.h>
#include <sys/idt.h>
#include "memorymanager.h"
#include "virtmemorymanager.h"
#include "scheduler.h"
#include "elf_header.h"
#include "directory.h"

#define INITIAL_STACK_SIZE 4096
char stack[INITIAL_STACK_SIZE];
uint32_t* loader_stack;
extern char kernmem, physbase;

extern int printf(const char *format,...);
extern void putchar(char);
extern void pic_remap();

static	uint32_t memory_map[MMAP_SIZE];
static	unsigned char page_reference[32767];
static	uint32_t virt_memory_map[VIRT_MMAP_SIZE];

struct page_table_mem kernel_page_tables = {NULL};
struct task_struct* kernel_task_struct;

uint64_t loader_cr3;
uint64_t cr3;

void load_pml4()
{
     cr3 = (uint64_t)kernel_page_tables.pml4e;
     asm volatile( "cli");
     asm volatile("movq %0, %%cr3":: "b"(cr3));
     asm volatile( "sti");
}

void divide_by_zero()
{
    int i=0,b=3,c;
    c=b/i;
    printf("%d\n",c);
}
void start(uint32_t* modulep, void* physbase, void* physfree)
{
	uint64_t *adr;
    struct smap_t {
		uint64_t base, length;
		uint32_t type;
	}__attribute__((packed)) *smap;
	mm_init(MEM_SIZE_BYTES,memory_map);
	mm_init_pageref(MEM_SIZE_BYTES,page_reference);
	vmm_init(VIRT_MEM_SIZE_BYTES+5242879,virt_memory_map);
	while(modulep[0] != 0x9001) modulep += modulep[1]+2;
	for(smap = (struct smap_t*)(modulep+2); smap < (struct smap_t*)((char*)modulep+modulep[1]+2*4); ++smap) {
        if (smap->type == 1 /* memory */ && smap->length != 0) {
			//printf("Available Physical Memory [%x-%x]\n", smap->base, smap->base + smap->length);
			mm_init_region(smap->base,smap->length);
		}
	}
	adr = (uint64_t*)physbase;
	mm_uninit_region((uint64_t)0,(uint64_t)0x400000);
	vmm_init_region((uint64_t)0,(uint64_t)0x7fb00000);
	for(adr = (uint64_t*)physbase;adr<=(uint64_t*)physfree;adr++) 
	{
     mmap(adr,(uint64_t*)(VIRT_KERNEL_BASE+(uint64_t)adr),&kernel_page_tables);
    }
    //printf("Kernel Mapping Done .. Now mapping video memory\n");
    adr = (uint64_t*)0xb8000; // video memory
    mmap(adr,(uint64_t*)(VIRT_VIDEO_MEM),&kernel_page_tables);
    load_pml4();
    
    kernel_task_struct = (struct task_struct*)pcb_malloc(200); // just allocate 1 page
    kernel_task_struct->kernel_stack = kernel_task_struct->stack = (uint64_t*)stack;
    kernel_task_struct->pid = 0;
    kernel_task_struct->cr3 = cr3;
    
    adr = (uint64_t*)0xffffffff80200000;
    
    process_queue[0]=(uint64_t)kernel_task_struct;
    run_queue[0]=0;
    process_count=0;
    
    //printf("SELF REF ADDR: %p\n",PGTBL_ADDR);
    //printf("SBUnix V.0.0\n");
    current_directory[0]=0;
    tarfs_initial_parse();
    //printf("rootfs - %d\n",getIndex(""));
    //tarfs_list_all();
    /*int dd=tarfs_opendir("lib/"),t;
    while(1) {
                              t=tarfs_readdir(dd);if(!t) break;
                              tarfs_printentry(t);
                             }
    */
    asm volatile("cli");
    //uint64_t loc = getLocation("bin/");
   
    uint64_t zombieKller = getLocation("bin/zombie_killer");
    
    //uint64_t loc_hello2 = getLocation("bin/hello");
    uint64_t loc_hello3 = getLocation("bin/sh");
    //uint64_t loc_hello4 = getLocation("bin/hello3");
    char* argv[10]={};
	char* envp[10]={"PATH=bin;lib"};
    //printf("bin = %p bin/hello3 = %p bin/hello4=%p\n",loc,loc_hello3,loc_hello4);
    //printf("NO PF TILL NOW\n");
    create_process((char*)zombieKller,"bin/zombie_killer",argv,envp);
    create_process((char*)loc_hello3,"bin/sh",argv,envp);
    
    foregroundProcess=2;
    //create_process((char*)loc_hello2,"bin/hello",argv,envp);
    //create_process((char*)loc_hello4,"bin/hello3",argv,envp);
    
    //asm volatile( "cli");
    
    asm volatile("mov $0x2b,%ax");
    asm volatile("ltr %ax");
    //asm volatile( "cli");
    
	// kernel starts here
	asm volatile("sti");
	while(1) {
              //printf("Woo NO PF TILL NOW\n");
              asm volatile("int $0x90;");
              //int i,j;
              //for(i=0;i<100000000;i++);
              //for(j=0;j<=3;j++) printf("%p ",process_queue[j]);
              //printf("\n");
             }
}

void boot(void)
{
	// note: function changes rsp, local stack variables can't be practically used
	register char *temp1, *temp2;
	//printf("DEBUG: Before ASM\n");
	__asm__(
		"movq %%rsp, %0;"
		"movq %1, %%rsp;"
		:"=g"(loader_stack)
		:"r"(&stack[INITIAL_STACK_SIZE])
	);
	//printf("DEBUG: After ASM\n");
	reload_gdt();
	reload_idt();
		
	pic_remap(0x20,0x28);
	setup_tss();
	
	
    
	start(
		(uint32_t*)((char*)(uint64_t)loader_stack[3] + (uint64_t)&kernmem - (uint64_t)&physbase),
		&physbase,
		(void*)(uint64_t)loader_stack[4]
	);
	for(
		temp1 = "!!!!! start() returned !!!!!", temp2 = (char*)0xb8000;
		*temp1;
		temp1 += 1, temp2 += 2
	) *temp2 = *temp1;
	while(1);
}
