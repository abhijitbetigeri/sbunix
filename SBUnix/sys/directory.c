#include <defs.h>
#include <sys/gdt.h>
#include <sys/tarfs.h>
#include "scheduler.h"
#include "memorymanager.h"
#include "virtmemorymanager.h"
#include "elf_header.h"
#include "directory.h"
extern int printf(const char *format,...);
extern int pow(int a,int b);
extern int atoi(char* a);
extern int otod(int octal);
extern int equal(char* a, char* b);
int strclean(char* a)
{
    char b[300];
    int i,j=0;
    for(i=0;a[i]!=0;i++) {
                          if(a[i]=='\b' && j>0) j--;
                          else {b[j]=a[i];j++;}
                         }
    b[j]=0;
    strcpy(a,b);
    return j;
}
void strcpy(char* a,char*b)
{
     int i=0;
     while(b[i]) {a[i]=b[i];i++;}
     a[i]=0;
}

void strappend(char* a,char*b)
{
     int i=0,j=0;
     while(a[i]) i++;
     while(b[j]) {a[i]=b[j];i++;j++;}
     a[i]=0;
}

int getIndex(char* s)
{
    int i;
    for(i=3;i<dir_desc_count;i++) if(equal(s,dir_desc_name[i])) return i;
    return 0; // not found
}
int getParent(char* c)
{
    int last_slash,last_slash2,i,copyend;
    char p[100];
    p[0]=0;
    last_slash=last_slash2=-1;
    for(i=0;c[i]!=0;i++) {
                         if(c[i]=='/') {
                                        last_slash2=last_slash;
                                        last_slash=i;
                                       }
                        }
    if(c[last_slash+1]) copyend=last_slash; else copyend=last_slash2;
    for(i=0;i<=copyend;i++) p[i]=c[i];
    p[i]=0;
    //printf("Parent: %s\n",p);
    if(p[0]==0) return 3;
    return getIndex(p);
}
void tarfs_initial_parse()
{
    dir_desc_count = 4;
    int size = sizeof(struct posix_header_ustar),size2,i;
    char * zddr;
    zddr = &_binary_tarfs_start;
    struct posix_header_ustar* star;
    dir_desc_addr[3]=0; // rootfs directory
    dir_desc_size[3]=0;
    dir_desc_read_pointer[3]=0;
    dir_desc_name[3][0]=0;
    dir_desc_parent[3]=0;
    while(1)
    {
     star = (struct posix_header_ustar*)zddr;
     size2 = otod(atoi(star->size));
     if(size2%512) size2=((size2+512)/512)*512;
     if(!equal(star->name,"")) 
     {
      //printf("%s %d %d\n",star->name,size,size2);
      dir_desc_addr[dir_desc_count]=(uint64_t)(zddr+size);
      dir_desc_size[dir_desc_count]=otod(atoi(star->size));
      dir_desc_read_pointer[dir_desc_count]=0;
      strcpy(dir_desc_name[dir_desc_count],star->name);
      dir_desc_parent[dir_desc_count]=0;
      if(dir_desc_size[dir_desc_count]>0) {
                                           char* a=(char*)dir_desc_addr[dir_desc_count];
                                           //printf("%c %c %c %c\n",*a,*(a+1),*(a+2),*(a+3));
                                           if((*a==0x7F) && (*(a+1)=='E') && (*(a+2)=='L') && (*(a+3)=='F')) dir_desc_type[dir_desc_count]=1;
                                           else if((*a=='#') && (*(a+1)=='!')) dir_desc_type[dir_desc_count]=2;
                                           else dir_desc_type[dir_desc_count]=3;
                                          }
      else dir_desc_type[dir_desc_count]=0;
      dir_desc_count++;
     }
     zddr+=size+size2;
     if(zddr>&_binary_tarfs_end) break;
    }
    dir_desc_parent[0]=dir_desc_parent[1]=dir_desc_parent[2]=-1;
    for(i=4;i<dir_desc_count;i++) dir_desc_parent[i]=getParent(dir_desc_name[i]);
}
void tarfs_list_all()
{
     int i=0;
     for(i=3;i<dir_desc_count;i++) {
                                    printf("%d %d %s %p %d %d\n",i,dir_desc_parent[i],dir_desc_name[i],dir_desc_addr[i],dir_desc_size[i],dir_desc_type[i]);
     }
}
int tarfs_opendir(char* dir) {
    int a = getIndex(dir);
    if(a && dir_desc_size[a]==0) return a;
    return -1;
}
int tarfs_readdir(int index) {
    int p = dir_desc_read_pointer[index];
    while(p<dir_desc_count) {
                             p++;
                             if(dir_desc_parent[p]==index) {
                                                            dir_desc_read_pointer[index]=p;
                                                            return p;
                                                           }
                            }
    return 0;
}
int tarfs_closeddir(int index) {
    if(index>=dir_desc_count) return -1;
    dir_desc_read_pointer[index]=0;
    return 1;
}
int tarfs_openfile(char* file) {
    int a = getIndex(file);
    if(a && dir_desc_type[a]>=2) return a; // should not be executable
    return -1;
}
void tarfs_printentry(int index) {
    if(index>=dir_desc_count) return;
    char *str = dir_desc_name[index];
    int last_slash,last_slash2,i,ps;
    last_slash=last_slash2=-1;
    for(i=0;str[i]!=0;i++) {
                         if(str[i]=='/') {
                                        last_slash2=last_slash;
                                        last_slash=i;
                                       }
                        }
    if(str[last_slash+1]==0) ps=last_slash2+1; else ps=last_slash+1;
    for(i=ps;str[i];i++) printf("%c",str[i]);
    printf("\n");
}
