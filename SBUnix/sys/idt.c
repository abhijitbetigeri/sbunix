#include "sys/idt.h"

#define MAX_IDT 256

extern void _isr_000();
extern void _isr_013();
extern void _isr_014();
extern void _isr_032();
extern void _isr_033();
extern void _isr_128(); // system calls
extern void _isr_144(); // putchar
extern void _isr_145(); // exit
extern int printf(const char *format,...);

/* Reference: https://code.google.com/p/shovelos/source/browse/trunk/kernel/arch/x86_64/?r=186 */

struct idt_t {

    uint16_t offset_0_15;
    uint16_t selector;
    unsigned ist : 3 ;
    unsigned reserved0 : 5;
    unsigned type : 4;
    unsigned zero : 1;
    unsigned dpl : 2;
    unsigned p : 1;
    uint16_t offset_16_31;
    uint32_t offset_63_32;
    uint32_t reserved1;

} __attribute__((packed));

static struct idt_t idt[MAX_IDT];

struct idtr_t {
	uint16_t size;
	uint64_t addr;
}__attribute__((packed));

struct idtr_t idtr = {
    sizeof(idt),
    (uint64_t)idt,
};
void _x86_64_asm_lidt(struct idtr_t* idtr);

/*void dummy()
{
     printf("INTERRUPT CAUGHT\n");
}*/
void reload_idt() {
	struct idt_t idt0 = PRESENT_ISR(0x08, 0, INTERRUPT , 0, ((uint64_t)&_isr_000));
	struct idt_t idt13 = PRESENT_ISR(0x08, 0, INTERRUPT , 0, ((uint64_t)&_isr_013));
	struct idt_t idt14 = PRESENT_ISR(0x08, 0, INTERRUPT , 0, ((uint64_t)&_isr_014));
	struct idt_t idt32 = PRESENT_ISR(0x08, 0, INTERRUPT , 0, ((uint64_t)&_isr_032));
	struct idt_t idt33 = PRESENT_ISR(0x08, 0, INTERRUPT , 0, ((uint64_t)&_isr_033));
	struct idt_t idt128 = PRESENT_ISR(0x08, 0, INTERRUPT , 3, ((uint64_t)&_isr_128)); // for system calls
	struct idt_t idt144 = PRESENT_ISR(0x08, 0, INTERRUPT , 3, ((uint64_t)&_isr_144)); // for putchar
	struct idt_t idt145 = PRESENT_ISR(0x08, 0, INTERRUPT , 3, ((uint64_t)&_isr_145)); // for exit
	idt[0] = idt0;
	idt[13] = idt13;
	idt[14] = idt14;
    idt[32] = idt32;
    idt[33] = idt33;
    idt[128] = idt128;
    idt[144] = idt144;
    idt[145] = idt145;
	_x86_64_asm_lidt(&idtr);
}
