#ifndef DIR_H
#define DIR_H
#define MAX_ENTRIES 50
int dir_desc_count;
uint64_t dir_desc_addr[MAX_ENTRIES];
uint32_t dir_desc_size[MAX_ENTRIES];
char dir_desc_name[MAX_ENTRIES][100];
unsigned char dir_desc_type[MAX_ENTRIES]; // 0-directory, 1-executable file, 2-script, 3-others
uint32_t dir_desc_read_pointer[MAX_ENTRIES];
uint32_t dir_desc_parent[MAX_ENTRIES];
char current_directory[100];
void tarfs_initial_parse();
void tarfs_list_all();
int tarfs_opendir(char*); // accepts directory name and returns an index into directory descriptor
int tarfs_readdir(int); 
int tarfs_closeddir(int);
void tarfs_printentry(int index); 
int getIndex(char* s);
int getParent(char* c);
int tarfs_openfile(char* file);

void strcpy(char* a,char*b);
void strappend(char* a,char* b);
int strclean(char* a); // removes backspaces
#endif
