#ifndef MMNGR_H
#define MMNGR_H

#define PT_PRESENT_FLAG                 (1<<0) // 0x01
#define PT_WRITABLE_FLAG                (1<<1) // 0x02
#define PT_USER_FLAG                    (1<<2) // 0x04
#define PT_WRITE_THROUGH_FLAG   (1<<3) // 0x08
#define PT_TERMINAL_FLAG        (1<<7) // 0x80
#define PT_GLOBAL_FLAG          (1<<8) // 0x100
#define PT_COW                  (1<<10)

#define VIRT_KERNEL_BASE 0xFFFFFFFF80000000
#define VIRT_VIDEO_MEM 0xFFFFFFFF80300000

#define MEM_SIZE_BYTES 134209536
#define MMAP_SIZE 1024
#define PAGE_TABLE_ALIGNLENT 0x1000
#define ALIGN_DOWN(x)   ((x) & ~(PAGE_TABLE_ALIGNLENT-1)) 

struct page_table_mem {

        //struct ticket_lock lock;

        uint64_t *pml4e;
        //uint64_t table_stack_top;
        //uint64_t table_stack_bottom;
};

void mm_init_region (uint64_t,uint64_t);
void mm_uninit_region (uint64_t,uint64_t);
void mm_init(uint64_t,uint32_t*);
void mm_init_pageref(uint64_t,unsigned char*);
inline char mm_isset(int);
inline unsigned char get_ref_cnt(int);
inline void mm_inc_ref_cnt(int);
inline void mm_dec_ref_cnt(int);
void mm_print_attributes();
uint64_t * mm_alloc_block();
uint64_t * mm_alloc_blocks(uint32_t);
void mm_free_block(uint64_t* p);
int mmap(uint64_t*, uint64_t*, struct page_table_mem*);

#endif
