#ifndef SCHDLR_H
#define SCHDLR_H
#define MAX_ENTRIES 50
#define MAX_PROCESS_COUNT 1000
#define yield() __asm volatile("int $0x90")
/*#define switch_to(prev,next) do {                                       \
        asm volatile( "pushq %rax");                                    \
        asm volatile( "pushq %rbx");                                    \
        asm volatile( "pushq %rcx");                                    \
        asm volatile( "pushq %rdx");                                    \
        asm volatile(                                                   \
		"movq %%rsp, %0;"                                               \
		:"=g"(((struct task_struct*)process_queue[prev])->rsp)          \
	    );                                                              \
    	asm volatile( "cli");                                           \
        asm volatile("movq %0, %%cr3":: "b"(cr3_queue[next]));          \
        asm volatile(                                                   \
    		"movq %0, %%rsp;"                                           \
    		:                                                           \
    		:"r"(((struct task_struct*)process_queue[next])->rsp)       \
    	);                                                              \
        asm volatile( "popq %rdx");                                     \
	    asm volatile( "popq %rcx");                                     \
	    asm volatile( "popq %rbx");                                     \
	    asm volatile( "popq %rax");                                     \
} while (0)*/

// source: ftp://ftp.acer.it/gpl/AS1800/linux-2.4.27/include/linux/mm.h

#define VM_READ		0x00000001	/* currently active flags */
#define VM_WRITE	0x00000002
#define VM_EXEC		0x00000004
#define VM_SHARED	0x00000008

#define VM_MAYREAD	0x00000010	/* limits for mprotect() etc */
#define VM_MAYWRITE	0x00000020
#define VM_MAYEXEC	0x00000040
#define VM_MAYSHARE	0x00000080

#define VM_GROWSDOWN	0x00000100	/* general info on the segment */
#define VM_GROWSUP	0x00000200
#define VM_SHM		0x00000400	/* shared memory area, don't swap out */
#define VM_DENYWRITE	0x00000800	/* ETXTBSY on write attempts.. */

#define VM_EXECUTABLE	0x00001000
#define VM_LOCKED	0x00002000
#define VM_IO           0x00004000	/* Memory mapped I/O or similar */

					/* Used by sys_madvise() */
#define VM_SEQ_READ	0x00008000	/* App will access data sequentially */
#define VM_RAND_READ	0x00010000	/* App will not benefit from clustered reads */

#define VM_DONTCOPY	0x00020000      /* Do not copy this vma on fork */
#define VM_DONTEXPAND	0x00040000	/* Cannot expand with mremap() */
#define VM_RESERVED	0x00080000	/* Don't unmap it from swap_out */

#define VM_STACK_FLAGS	0x00000177

#define PER_PROCESS_LIMIT 50

struct task_struct{
    uint64_t* stack; // its own stack
    uint64_t* kernel_stack; // its kernel stack
    uint32_t parent; // its parent
    //uint64_t counter; //how long its run;
    uint64_t pid; //my id;
    char procName[100];
    uint32_t priority; // when should it run
    uint64_t* rsp;
    uint64_t* kernel_rsp;
    //uint64_t* bump_ptr;// pointer for bump allocation
    uint64_t cr3; // base address of PML4E
    uint64_t pml4_vaddr; // virtual address of PML4 table. Required for freeing the PML4 table while killing the process
    //char state; //0 runnable ,-1 unrunnable, >0 stopped
    char is_zombie;
    char exit_status;
    struct mm_struct *mm;
    uint64_t ulimit; // size of stack
    char stdin[1024];
    int last_read;
    int last_write;
    char stderr[512];
    int last_eread;
    int last_ewrite;
    char tarfs_desc[MAX_ENTRIES];
    uint32_t tarfs_read_ptr[MAX_ENTRIES];
};
struct mm_struct {
        struct vm_area_struct  *mmap;               /* list of memory areas */
        int                    map_count;           /* number of memory areas */
        //struct list_head       mmlist;              /* list of all mm_structs */
        uint64_t          start_code;          /* start address of code */
        uint64_t          end_code;            /* final address of code */
        uint64_t          start_data;          /* start address of data */
        uint64_t          end_data;            /* final address of data */
        uint64_t          start_brk;           /* start address of heap */
        uint64_t          brk;                 /* final address of heap */
};
struct vm_area_struct {
        struct mm_struct             *vm_mm;        /* associated mm_struct */
        uint64_t                vm_start;      /* VMA start, inclusive */
        uint64_t                vm_end;        /* VMA end , exclusive */
        struct vm_area_struct        *vm_next;      /* list of VMA's */
        //pgprot_t                     vm_page_prot;  /* access permissions */
        uint64_t                vm_flags;      /* flags */
        uint64_t                vm_pgoff;          /* offset within file */
        uint64_t                vm_memsz;          /* mapped file, if any */
        uint64_t                vm_filestart;
        //void                         *vm_private_data;  /* private data */
};

uint64_t process_queue[MAX_PROCESS_COUNT];
uint32_t run_queue[MAX_PROCESS_COUNT];
uint32_t sleep_time[MAX_PROCESS_COUNT];
int waiting_on[MAX_PROCESS_COUNT];
char waiting_to_read[MAX_PROCESS_COUNT];
int waitpid_ret_pid;
int waitpit_ret_exit_st;
uint32_t process_count;
int create_process(char*,char*,char**,char**);
void first_ctx_sw(int);
void fun1();
void fun2();
void fun3();
void schedule();
void schedule2();
void get_exit_status(uint64_t);
void exit_current_process();
struct task_struct* getCurrentTaskStruct(); // returns the current queue index
int getCurrentPID();
uint32_t forkCurrentProcess();
uint32_t execvpeCurrentProcess(uint64_t,uint64_t,uint64_t);
void createForkedProcess();
uint64_t getLocation(char* prog); // returns the tarfs address of a given executable. 0 if not found.
uint32_t sleepCurrentProcess(uint32_t);
void getWaitPid(uint64_t,uint64_t);

uint32_t foregroundProcess;
uint64_t doRead(uint64_t,uint64_t,uint64_t);
uint64_t doWrite(uint64_t,uint64_t,uint64_t);

uint64_t doOpenDir(uint64_t);
uint64_t doReadDir(uint64_t);
uint64_t doCloseDir(uint64_t);
void doPrintDirEnt(uint64_t);
void getCurrentDirectory(uint64_t);
void setDirectory(uint64_t);

uint64_t doOpenFile(uint64_t);
uint64_t doCloseFile(uint64_t);
void listCurrentProcesses();
#endif
