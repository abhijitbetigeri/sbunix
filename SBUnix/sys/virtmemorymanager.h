#ifndef VMMNGR_H
#define VMMNGR_H

#include "scheduler.h"

#define VIRT_MEM_SIZE_BYTES 0x7fb00000 // 0xffffffff80500000 to 0xffffffffffffffff
#define VIRT_MMAP_SIZE 16384 // We are mapping a 2GB virtual address space which is 524288 pages long. This requires
                             // 524288 bits or 16384 32-bit entries

#define VIRT_PCB_MEM 0xFFFFFFFF80500000

#define PT_PRESENT_FLAG                 (1<<0) // 0x01
#define PT_WRITABLE_FLAG                (1<<1) // 0x02
#define PT_USER_FLAG                    (1<<2) // 0x04
#define PT_WRITE_THROUGH_FLAG   (1<<3) // 0x08
#define PT_TERMINAL_FLAG        (1<<7) // 0x80
#define PT_GLOBAL_FLAG          (1<<8) // 0x100

#define PAGE_TABLE_ALIGNLENT 0x1000
#define ALIGN_DOWN(x)   ((x) & ~(PAGE_TABLE_ALIGNLENT-1)) 

// Reference: http://forum.osdev.org/viewtopic.php?f=1&p=176913
#define RECURSIVE_SLOT (500L)

#define L4_SHIFT (39)
#define L3_SHIFT (30)
#define L2_SHIFT (21)
#define L1_SHIFT (12)

#define UPPER_ADDR(x) ((uint64_t*)(0xffffL<<48|(x)))

#define PGTBL_ADDR UPPER_ADDR((RECURSIVE_SLOT<<L4_SHIFT))

#define PGDIR_ADDR UPPER_ADDR((RECURSIVE_SLOT<<L4_SHIFT) \
                     |(RECURSIVE_SLOT<<L3_SHIFT))

#define PDPT_ADDR UPPER_ADDR((RECURSIVE_SLOT<<L4_SHIFT) \
                     |(RECURSIVE_SLOT<<L3_SHIFT) \
                     |(RECURSIVE_SLOT<<L2_SHIFT))

#define PML4T_ADDR UPPER_ADDR((RECURSIVE_SLOT<<L4_SHIFT) \
                     |(RECURSIVE_SLOT<<L3_SHIFT) \
                     |(RECURSIVE_SLOT<<L2_SHIFT) \
                     |(RECURSIVE_SLOT<<L1_SHIFT))

void vmm_init_region (uint64_t,uint64_t);
void vmm_uninit_region (uint64_t,uint64_t);
void vmm_init(uint64_t,uint32_t*);
inline char vmm_isset(int);
void vmm_print_attributes();
uint64_t * vmm_alloc_block();
uint64_t * vmm_alloc_blocks(uint32_t);
void vmm_free_blocks(uint64_t*,uint32_t);
int vmm_first_free();

volatile uint64_t* getPML4E(uint64_t*);
volatile uint64_t* getPDP(uint64_t*);
volatile uint64_t* getPDPE(uint64_t*);
volatile uint64_t* getPD(uint64_t*);
volatile uint64_t* getPDE(uint64_t*);
volatile uint64_t* getPT(uint64_t*);
volatile uint64_t* getPTE(uint64_t*);

uint64_t* pcb_malloc(uint32_t);
void pcb_free(uint64_t*,uint32_t); // size is used to calculate the number of blocks we need to free

void free_page(uint64_t*);

uint32_t back_with_physical(uint64_t*);
uint32_t back_with_physical_addr(uint64_t*);
void alloc_mm_struct(struct task_struct*);
struct vm_area_struct * alloc_vma(struct mm_struct*);
void get_malloc_size(uint64_t);
uint64_t smalloc();
uint64_t sbrk1(); //For first heap
uint64_t sbrk2(); //For second heap

#endif
