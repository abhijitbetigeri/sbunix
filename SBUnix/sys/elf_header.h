#ifndef _ELF_H
#define _ELF_H

// program header "type" field
#define PT_NULL	0
#define PT_LOAD	1
#define PT_DYNAMIC	2
#define PT_INTERP	3
#define PT_NOTE	4
#define PT_SHLIB	5
#define PT_PHDR	6
#define PT_TLS	7
#define PT_LOOS	0x60000000
#define PT_HIOS	0x6fffffff
#define PT_LOPROC	0x70000000
#define PT_HIPROC	0x7fffffff

// program header "flags" field
#define PF_X	0x1
#define PF_W	0x2
#define PF_R	0x4

struct elf_header
{
       char magic[4];
       char ident[5];
       char unused[7];
       uint16_t type;
       uint16_t machine;
       uint32_t version;
       uint64_t entry;
       uint64_t phoff;
       uint64_t shoff;
       uint32_t flags;
       uint16_t ehsize;
       uint16_t phentsize;
       uint16_t phnum;
       uint16_t shentsize;
       uint16_t shnum;
       uint16_t shstrndx;
};

struct prog_header
{
       uint32_t p_type;                        /* Segment type */
       uint32_t p_flags;                /* Segment flags */
       uint64_t p_offset;                /* Segment file offset */
       uint64_t p_vaddr;                /* Segment virtual address */
       uint64_t p_paddr;                /* Segment physical address */
       uint64_t p_filesz;                /* Segment size in file */
       uint64_t p_memsz;                /* Segment size in memory */
       uint64_t p_align;                /* Segment alignment */
};

#endif
