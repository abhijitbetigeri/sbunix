#include<syscall.h>
#include<defs.h>

extern int printf(const char *format,...);
void exit(int status) {
  __syscall1(SYS_EXIT,(uint64_t)status);
}
