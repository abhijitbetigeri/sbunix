#include<syscall.h>
#include<defs.h>

void* malloc(uint64_t bytes) {
  uint64_t a = __syscall1(SYS_MALLOC,bytes);
  dummy_call(a);
  return (void*)a;
}
