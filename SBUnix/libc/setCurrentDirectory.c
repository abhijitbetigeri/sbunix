#include<syscall.h>
#include<defs.h>

void setCurrentDirectory(char* dir) {
  __syscall1(SYS_SETCURRDIR,(uint64_t)dir);
}
