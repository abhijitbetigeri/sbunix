#include<syscall.h>
#include<defs.h>

unsigned int sleep(unsigned int time) {
  unsigned int ret = __syscall1(SYS_SLEEP,(uint64_t)time);
  dummy_call(ret);
  return ret;
}
