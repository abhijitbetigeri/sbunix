#include<syscall.h>
#include<defs.h>

int open(char* fname) {
  unsigned int ret = __syscall1(SYS_OPENFILE,(uint64_t)fname);
  dummy_call(ret);
  return ret;
}
