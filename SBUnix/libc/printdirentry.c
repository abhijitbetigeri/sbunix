#include<syscall.h>
#include<defs.h>

int printdirentry(int dd) {
  unsigned int ret = __syscall1(SYS_PRINTDIRENT,(uint64_t)dd);
  dummy_call(ret);
  return ret;
}
