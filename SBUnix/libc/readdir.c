#include<syscall.h>
#include<defs.h>

int readdir(int dd) {
  unsigned int ret = __syscall1(SYS_READDIR,(uint64_t)dd);
  dummy_call(ret);
  return ret;
}
