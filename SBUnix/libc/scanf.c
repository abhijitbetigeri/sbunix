#include<syscall.h>
#include<stdio.h>
#include<defs.h>

/* Reference: http://wiki.osdev.org/Printing_To_Screen#printf_and_variable_argument_lists */
#define va_start(v,l) __builtin_va_start(v,l)
#define va_arg(v,l)   __builtin_va_arg(v,l)
#define va_end(v)     __builtin_va_end(v)
#define va_copy(d,s)  __builtin_va_copy(d,s)
typedef __builtin_va_list va_list;
int pos=0;
int assigned=0;
char getc(char* s)
{
     char is_init=0,c;
     //printf("NO SEGF. pos=%d\n",pos);
     while(1) {
               if(s[pos]=='\0' && !is_init) return -1;
               if(s[pos]==' ' || s[pos]=='\t') {pos++;continue;}
               c=s[pos];is_init=1;assigned++;
               while(s[pos]!=' ' && s[pos]!='\t' && s[pos]!='\0') pos++;
               return c;
              }
     return -1;
}
void gets(char* rs,char* s)
{
     int i=0;
     char is_init=0;
     //printf("NO SEGF. pos=%d\n",pos);
     while(1) {
               if(s[pos]=='\0' && !is_init) {rs[i]=0;return;}
               if(s[pos]==' ' || s[pos]=='\t') {pos++;continue;}
               while(s[pos]!=' ' && s[pos]!='\t' && s[pos]!='\0') {
                               rs[i++]=s[pos];
                               pos++;
               }
               rs[i]=0;is_init=1;assigned++;return;
               //printf("here ");
              }
}

int getint(char* s)
{
    char is_init=0;
    int sig=1,n=0;
    while(1) {
              if(s[pos]=='\0' && !is_init) return -1;
              if(s[pos]==' ' || s[pos]=='\t') {pos++;continue;}
              if(s[pos]=='-') {sig=-1;pos++;}
              while(s[pos]!=' ' && s[pos]!='\t' && s[pos]!='\0') {
                              //printf("%c-%d ",s[pos],n);
                              n=n*10+s[pos]-'0';
                              pos++;
              }
              n=n*sig;
              //printf("%d %d\n",sig,n);
              is_init=1;
              assigned++;
              return n;
             }
    return -1;
}
int scanf(const char *format,...)
{
    int i;
    
    int *a;
    char* c,*str;
    char s[110];
    read(0,s,100);
    va_list arguments;
    va_start(arguments,format);
    pos=0;
    assigned=0;
    for(i=0;format[i]!=0;i++)
    {
     if(format[i]=='%') {
                           i++;
                           if(format[i]=='c')
                           {
                            c=va_arg(arguments,char*);
                            *c=getc(s);
                           }
                           else if(format[i]=='d')
                           {
                            a = va_arg(arguments,int*);
                            *a=getint(s);
                           }
                           else if(format[i]=='s')
                           {
                            str=va_arg(arguments,char*);
                            gets(str,s);
                           }
     }
    }
        
    va_end(arguments);
    return assigned;
}
