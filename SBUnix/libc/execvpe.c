#include<syscall.h>
#include<stdio.h>
#include<defs.h>

int execvpe(const char *file, char *argv[], char * envp[]) {
  //printf("EXECVPE: Passing arguments: %p %p %p\n",file,argv,envp);
  uint64_t a = __syscall3(SYS_EXECVPE,(uint64_t)file,(uint64_t)argv,(uint64_t)envp);
  dummy_call(a);
  return (int)a;
}
