#include<syscall.h>
#include<defs.h>

int close(int dd) {
  unsigned int ret = __syscall1(SYS_CLOSEFILE,(uint64_t)dd);
  dummy_call(ret);
  return ret;
}
