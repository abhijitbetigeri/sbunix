#include<syscall.h>
#include<defs.h>

int opendir(char* dir) {
  unsigned int ret = __syscall1(SYS_OPENDIR,(uint64_t)dir);
  dummy_call(ret);
  return ret;
}
