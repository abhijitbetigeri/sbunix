#include<syscall.h>
#include<defs.h>

int getpid() {
  int a = __syscall0(SYS_GETPID);
  //printf("DEBUG: getpid(): returning value: %d\n",a);
  dummy_call(a);
  return a;
}
