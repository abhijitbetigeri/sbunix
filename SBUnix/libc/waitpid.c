#include<syscall.h>
#include<defs.h>

unsigned int waitpid(int pid,int* status) {
  unsigned int ret = __syscall2(SYS_WAITPID,(uint64_t)pid,(uint64_t)status);
  dummy_call(ret);
  return ret;
}
