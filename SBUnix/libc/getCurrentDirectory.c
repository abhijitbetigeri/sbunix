#include<syscall.h>
#include<defs.h>

void getCurrentDirectory(char* dir) {
  __syscall1(SYS_GETCURRDIR,(uint64_t)dir);
}
