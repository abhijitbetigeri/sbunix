#include<syscall.h>
#include<defs.h>

int closedir(int dd) {
  unsigned int ret = __syscall1(SYS_CLOSEDIR,(uint64_t)dd);
  dummy_call(ret);
  return ret;
}
