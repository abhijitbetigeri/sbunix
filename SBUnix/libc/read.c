#include<syscall.h>
#include<defs.h>

int read(int source, char *buf, int nbytes) {
  //printf("EXECVPE: Passing arguments: %p %p %p\n",file,argv,envp);
  if(source==1) return -1; // Cannot read from stdout
  uint64_t a = __syscall3(SYS_READ,(uint64_t)source,(uint64_t)buf,(uint64_t)nbytes);
  dummy_call(a);
  return (int)a;
}
